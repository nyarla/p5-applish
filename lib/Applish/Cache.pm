package Applish::Cache;

use strict;
use Applish::Role;

my $TRUE    = !! 1;
my $FALSE   = !! 0;

requires qw(
    engine
    set         set_multi
    cas         cas_multi
    add         add_multi
    replace     replace_multi
    append      append_multi
    prepend     prepend_multi
    get         get_multi
    gets        gets_multi
    incr        incr_multi
    decr        decr_multi
    delete      delete_multi
    clear
);

has namespace => (
    is      => 'rw',
    isa     => 'Str',
    default => '',
);

sub set_multi {
    my ( $self, @tasks ) = @_;

    my @results = ();
    my %results = ();

    for my $task ( @tasks ) {
        my $ret = $self->set( @{ $task } );
        push @results, $ret;
        $results{$task->[0]} = $ret;
    }

    return @results if ( wantarray );
    return { %results };
}

sub cas_multi {
    my ( $self, @tasks ) = @_;

    my @results = ();
    my %results = ();

    for my $task ( @tasks ) {
        my $ret = $self->cas( @{ $task } );
        push @results, $ret;
        $results{$task->[0]} = $ret;
    }

    return @results if ( wantarray );
    return { %results };
}

sub add {
    my ( $self, $key, $value, $expire ) = @_;

    if ( ! defined $self->get($key) ) {
        my $ret = $self->set( $key, $value, $expire );
        return undef    if ( ! defined $ret );
        return $FALSE   if ( ! $ret );
        return $TRUE;
    }

    return $FALSE;
}

sub add_multi {
    my ( $self, @tasks ) = @_;

    my @results = ();
    my %results = ();

    for my $task ( @tasks ) {
        my $ret = $self->add( @{ $task } );
        push @results, $ret;
        $results{$task->[0]} = $ret;
    }

    return @results if ( wantarray );
    return { %results };
}

sub replace {
    my ( $self, $key, $value, $expire ) = @_;

    if ( defined $self->get( $key ) ) {
        my $ret = $self->set( $key, $value, $expire );
        return undef if ( ! defined $ret );
        return $FALSE if ( ! $ret );
        return $TRUE;
    }

    return $FALSE
}

sub replace_multi {
    my ( $self, @tasks ) = @_;

    my @results = ();
    my %results = ();

    for my $task ( @tasks ) {
        my $ret = $self->replace( @{ $task } );
        push @results, $ret;
        $results{$task->[0]} = $ret;
    }

    return @results if ( wantarray );
    return { %results };
}

sub append {
    my ( $self, $key, $value ) = @_;

    if ( ref $value ) {
        warn "Append value is not scalar: ${key} => ${value}";
        return undef;
    }

    my $source = $self->get($key);

    if ( ref $source ) {
        warn "Target value is not scalar: ${key} => ${source}";
        return undef;
    }

    return $self->set( $key, "${source}${value}" );
}

sub append_multi {
    my ( $self, @tasks ) = @_;

    my @results = ();
    my %results = ();

    for my $task ( @tasks ) {
        my $ret = $self->append( @{ $task } );
        push @results, $ret;
        $results{$task->[0]} = $ret;
    }

    return @results if ( wantarray );
    return { %results };
}

sub prepend {
    my ( $self, $key, $value ) = @_;

    if ( ref $value ) {
        warn "Prepend value is not scalar: ${key} => ${value}";
        return undef;
    }

    my $source = $self->get($key);

    if ( ref $source ) {
        warn "Target value is not scalar: ${key} => ${source}";
        return undef;
    }

    return $self->set( $key, "${value}${source}" );
}


sub prepend_multi {
    my ( $self, @tasks ) = @_;

    my @results = ();
    my %results = ();

    for my $task ( @tasks ) {
        my $ret = $self->prepend( @{ $task } );
        push @results, $ret;
        $results{$task->[0]} = $ret;
    }

    return @results if ( wantarray );
    return { %results };
}

sub get_multi {
    my ( $self, @keys ) = @_;
    my %results  = ();

    for my $key ( @keys ) {
        my $ret = $self->get($key);
        $results{$key} = $ret if ( defined $ret );
    }

    return { %results }
}

sub gets_multi {
    my ( $self, @keys ) = @_;
    my %results = ();

    for my $key ( @keys ) {
        $results{$key} = $self->gets($key);
    }

    return { %results };
}

sub incr {
    my ( $self, $key, $incr ) = @_;

    $incr = 1 if ( ! defined $incr );

    if ( $incr != int($incr) || $incr < 0 ) {
        warn "Increment value is not positive integer: ${incr}";
        return undef;
    }

    my $source = $self->get($key);

    if ( $source != int($source) ) {
        warn "Target value is not integer string.";
        return undef;
    }


    $self->set( $key, $source + $incr );
    
    return $self->get($key);
}

sub incr_multi {
    my ( $self, @tasks ) = @_;

    my @results = ();
    my %results = ();

    for my $task ( @tasks ) {
        $task = [ $task ] if ( ! ref $task );
        my $ret = $self->incr(@{ $task });
        push @results, $ret;
        $results{$task->[0]} = $ret;
    }

    return @results if ( wantarray );
    return { %results };
}

sub decr {
    my ( $self, $key, $decr ) = @_;

    $decr = 1 if ( ! defined $decr );

    if ( $decr != int($decr) || $decr < 0 ) {
        warn "Decrement value is not positive integer: ${decr}";
        return undef;
    }

    my $source  = $self->get($key);

    if ( $source != int($source) ) {
        warn "Target value is not integer string.";
        return undef;
    }

    $self->set( $key, $source - $decr );
    
    return $self->get($key);
}

sub decr_multi {
    my ( $self, @tasks ) = @_;

    my @results = ();
    my %results = ();

    for my $task ( @tasks ) {
        $task = [ $task ] if ( ! ref $task );
        my $ret = $self->decr(@{ $task });
        push @results, $ret;
        $results{$task->[0]} = $ret;
    }

    return @results if ( wantarray );
    return { %results };
}

sub delete_multi {
    my ( $self, @keys ) = @_;

    my @results = ();
    my %results = ();

    for my $key ( @keys ) {
        my $ret = $self->delete( $key );
        push @results, $ret;
        $results{$key} = $ret;
    }

    return @results if ( wantarray );
    return { %results };
}

__END_OF_ROLE__;

=head1 NAME

Applish::Cache - Abstract cache interface for Applish.

=head1 SYNPOSIS

    package MyCache;
    
    use strict;
    use Appish::Class;
    
    with qw( Applish::Cache );
    
    sub engine { 'MyCache' }
    
    sub set     {}
    sub cas     {}
    sub get     {}
    sub gets    {}
    sub delete  {}
    
    __END_OF_CLASS__;

=head1 DESCRIPTION

This role is abstract cache interface definition.

=head1 INTERFACE METHOD

=head2 namespace

    $cache->namespace('foo');
    
    my $ns = $cache->namespace;

This method sets or gets cache key namespace.

=head2 engine

    $cache->engine;

This method returns cache engine name.

=head2 set

    $cache->set( $key, $value, $expire );

This method stores C<$value> on the cache under C<$key>.

Argument C<$expire> is designated ecpoch time of cache expire time.

Return value:

=over

=item C<true>

Store cache is successed

=item C<false>

Store cache is failed.

=item C<undef>

Some error.

=back

=head2 set_multi

    $cache->set_multi(
        [ $key, $value ],
        [ $key, $value, $expire ]
    );

This method stores multi values.

Return value:

=over

=item In list context

In list context, this method returns C<@results>.

each C<$results[$index]> is the result value corresponding to the argument at position C<$index>.

=item In scalar context.

In scalar context, this method returns hash reference.

where C<$results-E<gt>{$key}> hols the result value.

=back

Plese see L<"set"> about return value means.

=head2 cas

    $cache->cas( $key, $cas, $value );
    $cache->cas( $key, $cas, $value, $expire );

This method stores C<$value> on the cache under C<$key>
if CAS (Consistent Access Storage) value associated with this key is equal to C<$cas>.

You can gets C<$cas> value using L<gets> or L<gets_multi>.

Return value means is equal to L<"set">.

=head2 cas_multi

    $cache->cas_multi(
        [ $key, $cas, $value ],
        [ $key, $cas, $value, $expire ],
        ...
    );

This method stores multi values if CAS value associated with this key is equal to C<$cas>.

Return value:

=over

Return value means are equal to L<"set_multi">.

=head2 add

    $cache->add( $key, $value );
    $cache->add( $key, $value, $expire );

This method stores C<$value> on the cache undef C<$key>
when C<$key> does not exists.

Return value means are equal to L<"set">

=head2 add_multi

    $cache->add_multi(
        [ $key, $value ]
        [ $key, $value, $expire ],
        ...
    );

This method stores multi values when a cache key does not exists.

Return value means are equal to L<"set_multi">

=head2 replace

    $cache->replace( $key, $value )
    $cache->replace( $key, $value, $expire )

This method stores C<$value> on the cache under C<$key>
when C<$key> does exists.

Return value means are equal to L<"set">

=head2 replace_multi

    $cache->replace(
        [ $key, $value ],
        [ $key, $value, $expire ],
    );

This method stores multi values when a cache key does exists.

Return value means are equal to L<"set_multi">

=head2 append

    my $value = 'foo';
    $cache->set( $key, $value );

    $cache->append( $key, 'bar' );
    
    $cache->get($key) # 'foobar'

This method appends C<$value> to currnet value on cache.

C<$key> and C<$value> should be scalars.

Return value means are equal to L<"set">.

=head2 append_multi

    $cache->append_multi(
        [ $key, $value ],
        [ $key, $value ],
        ...
    );

This method appends multi values to currnet values on cache.

Return value means are equal to L<"set_multi">

=head2 prepend

    my $value = 'foo';
    $cache->set( $key, $value );

    $cache->prepend( $key, 'bar' );
    
    $cache->get($key) # 'barfoo'

This method prepends C<$value> to currnet value on cache.

C<$key> and C<$value> should be scalars.

Return value means are equal to L<"set">.

=head2 append_multi

    $cache->prepends_multi(
        [ $key, $value ],
        [ $key, $value ],
        ...
    );

This method prepends multi values to currnet values on cache.

Return value means are equal to L<"set_multi">

=head2 get

    my $value = $cache->get($key);

This method gets a cache value.

Return value is a cache value, or nothing.

=head2 get_multi

    $cache->get_multi(@keys);

This methods gets cache values.

Return value is hash reference. where C<$results-E<gt>{$key}> holds corresponding value.

=head2 gets
    
    $cache->gets($key)

This method gets a CAS id.

Return value is C<[ $cas, $value ]> or nothing.

=head2 gets_multi

    $cache->gets_multi(@keys);

This method gets multi CAS ids.

Return value is C<{ $key =E<gt> [ $cas, $value ] }, ...>.

=head2 incr

    $cache->incr($key);
    $cache->incr($key, $increment);

This method increments a cache value.

Return value is new value of <$key> or false.

=head2 incr_multi

    $cache->incr_multi(
        $key,
        [ $key ],
        [ $key, $increment ],
    );

This method increments multi cache values.

Return value means are equal to L<"set_multi">

=head2 decr

    $cache->decr($key);
    $cache->decr($key, $decr);

This method decrements a cache value.

Return value is new value of <$key> or false.

=head2 decr_multi

    $cache->decr_multi(
        $key,
        [ $key ],
        [ $key, $decrement ],
    );

This method decrements multi cache values.

Return value means are equal to L<"set_multi">

=head2 delete

    $cache->delete($key);

This method deletes a cache value.

Return value means are equal to L<"set">.

=head2 delete_multi

    $cache->delete_multi(@keys);

This method deletes a cache value of keys.

Return value means are equal to L<"set_multi">.

=head2 clear

    $cache->clear;

This method clears all cache data.

=head1 REQUIREMENT METHOD

=head2 engine

=head2 set

=head2 set_multi (optional)

=head2 cas 

=head2 cas_multi (optional)

=head2 add (optional)

=head2 add_multi (optional)

=head2 append (optional)

=head2 append_multi (optional)

=head2 prepend (optional)

=head2 prepend_multi (optional)

=head2 get 

=head2 get_multi (optional)

=head2 gets

=head2 gets_multi (optional)

=head2 incr (optional)

=head2 incr_multi (optional)

=head2 decr (optional)

=head2 decr_multi (optional)

=head2 delete 

=head2 delete_multi (optional)

=head2 clear

=head1 AUTHOR

Naoki Okamura (Nyarla) E<lt>nyarla[ at ]thotep.netE<gt>

=head1 LICENSE

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut