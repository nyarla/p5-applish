package Applish::Test::PathUtils;

use strict;
use warnings;

use base qw( Exporter );

use File::Spec;
use FindBin ();

our ( $rootdir, $testdir, $examples );
our @EXPORT_OK = qw( $rootdir $testdir $examples );

{
    my @path = File::Spec->splitdir( $FindBin::Bin );
    while ( my $path = pop @path ) {
        if ( $path eq 't' ) {
            $rootdir    = File::Spec->catfile( @path );
            $testdir    = File::Spec->catfile( $rootdir, 't' );
            $examples   = File::Spec->catfile( $rootdir, 't', 'examples' );
        }
    }
}

1;

=head1 NAME

Applish::Test::PathUtils - Path exporter for Applish tests.

=head1 SYNPOSIS

    #!perl
    
    use strict;
    use warnings;
    
    use Test::More;
    use Applish::Test::PathUtils qw( $examples );
    
    ok( -e "${examples}/foo.pl" );
    
    done_testing;

=head1 DESCRIPTION

This class is a path utility for test files and directories.

=head1 EXPORTS

=head2 C<$rootdir>

A distribute root directory.

=head2 C<$testdir>

A tests directory.

=head2 C<$examples>

A example files directory.

=head1 AUTHOR

Naoki Okamura (Nyarla) E<lt>nyarla[ at ]thotep.netE<gt>

=head1 LICENSE

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut
