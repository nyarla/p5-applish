package Applish::Formats;

use strict;
use Applish::Class;
use Applish::Class::Engine qw( load_class is_class_loaded );
use Applish::Class::Finder qw( find_packages );
use Applish::Exceptions;

has support => (
    is          => 'rw',
    isa         => 'ArrayRef[Str]',
    trigger     => sub { $_[0]->clear_formatter },
    lazy_build  => 1,
);

sub _build_support {
    my ( $self ) = @_;
    my @modules;

    for my $module ( find_packages( namespace => 'Applish::Format' ) ) {
        $module =~ s{^Applish::Format::}{}g;
        push @modules, $module;
    }

    return [ @modules ];
}

has formatter => (
    is          => 'rw',
    isa         => 'HashRef[ClassName]',
    lazy_build  => 1,
);

sub _build_formatter {
    my ( $self ) = @_;
    my %formats = ();
    for my $format ( @{ $self->support } ) {
        my $module;
        if ( $format =~ m{^[+](.+)} ) {
            $module = $1;
        }
        else {
            $module = "Applish::Format::${format}";
        }
        
        load_class( $module ) if ( ! is_class_loaded( $module ) );
        
        $formats{$format} = $module;
    }

    return { %formats };
}

sub load {
    my ( $self, $format, $source ) = @_;
    
    Applish::Exception::NotSupported->throw( error => "This format (${format}) is not supported." )
        if ( ! exists $self->formatter->{$format} );

    return $self->formatter->{$format}->Load( $source );
}

sub dump {
    my ( $self, $format, $data ) = @_;
    
    Applish::Exception::NotSupported->throw( error => "This format (${format}) is not supported." )
        if ( ! exists $self->formatter->{$format} );

    return $self->formatter->{$format}->Dump( $data );
}

sub is_supported {
    my ( $self, $format ) = @_;
    return ( exists $self->formatter->{$format} ) ? 1 : 0 ;
}

sub detect_file_format {
    my ( $self, $path ) = @_;

    for my $format ( keys %{ $self->formatter } ) {
        if ( $self->formatter->{$format}->is_supported_file($path) ) {
            return $format;
        }   
    }

    return undef;
}

sub detect_load {
    my ( $self, $path, $source ) = @_;
    my $format = $self->detect_file_format( $path );

    Applish::Exception::NotSupported->throw( error => "Cannot detect file format: ${path}" )
        if ( ! defined $format );

    return $self->load( $format => $source );
}

__END_OF_CLASS__;

=head1 NAME

Applish::Formats - Manage file formats.

=head1 SYNPOSIS

    use Applish::Formats;
    
    my $loader = Applish::Formats->new(
        support => [qw( Perl YAML )],
    );
    
    $loader->load( $format => $source );
    my $data = $loader->dump( $format => $data );

=head1 DESCRIPTION

This class is manager for file formatters.

=head1 METHODS

=head2 new

    my $loader = Applish::Formats->new(
        support => [qw( Perl +MyFormat )],
    );

This method is constructor of this class.

Arguments:

=over

=item C<support>

This argument is ARRAY reference include your using file formats.

This argument is required.

Default value is C<[qw(Perl)]>.

class mapping:
    
    plan            => Applish::Format family
    +My::Module     => My::Module
    
    example:
    
    Perl            => Applish::Format::Perl
    +MyApp::Format  => MyApp::Format

=item C<formatter>

    $loader = Applish::Formats->new(
        formatter => {
            Perl    => 'Applish::Format::Perl',
            Blosxom => 'MyApp::Format::Blosxom',
        },
    );

This argument is HASH reference include format name and formatter class.

This argument is optional.

=back

=head2 load

    my $data = $loader->load( $format => $source );

This method parses C<$format> data from C<$source>.

=head2 dump

    my $text = $loader->dump( $format => $perldata );

This method dumps C<$perldata> as C<$format>.

=head2 is_supported

    my $bool = $loader->is_supported('Perl')

This method returns bool of desiganted file format is supported.

=head2 detect_file_format

    my $formatname = $loader->detect_file_format($filepath);

This method detects file format from file path.

If cannot detect format, this method returns undef.

=head2 detect_load

    my $data = $loader->detect_load( $path => $source );

This method is alias of as follow:

    my $format  = $loader->detect_file_format($path);
    my $data    = $loader->load( $format, $source );

=head1 AUTHOR

Naoki Okmaura (Nyarla) E<lt>nyarla[ at ]thotep.netE<gt>

=head1 SEE ALSO

L<Applish::Format>

=head1 LICENSE

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut
