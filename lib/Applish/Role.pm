package Applish::Role;

use strict;
use warnings;

use Applish::Class::Engine qw( any_moose );
use Applish::Exceptions;

my ( $engine, $metaclass, $roleclass );

BEGIN {
    $engine     = any_moose();
    $metaclass  = "${engine}::Meta::Role";
    $roleclass  = "${engine}::Role";

    require "${engine}/Role.pm"
}

sub init_role {
    my ( $target ) = @_;
    my $meta = $metaclass->initialize($target);

    no strict 'refs';
    no warnings 'redefine';

    *{"${target}::meta"} = sub { $meta };
}

sub end_of_role {
    my ( $target ) = @_;

    local $@;
    eval qq{
        package ${target};
        ${roleclass}->unimport();
    };

    Applish::Exception::EvaluateError->throw( error => "Cannot unimport Applish::Role: ${@}" )
        if ( $@ );

    no strict 'refs';
    delete ${"${target}::"}{'__END_OF_ROLE__'};

    return 1;
}

sub import {
    my $class   = shift;
    my $caller  = caller(0);

    return if ( $caller eq 'main' );

    strict->import;
    warnings->import;

    init_role($caller);

    no strict 'refs';
    *{"${caller}::__END_OF_ROLE__"} = sub {
        my $caller = caller(0);
        end_of_role($caller);
    };

    $roleclass->import({ into_level => 1 });

    return 1;
}

1;

=head1 NAME

Applish::Role - Role builder for Applish

=head1 SYNPOSIS

    package MyRole;
    
    use strict;
    use Applish::Role;
    
    requires qw( foo bar baz );
    
    __END_OF_ROLE__;

=head DESCRIPTION

This class is role builder for Applish.

This class alternate C<use Any::Moose '::Role'>.

=head1 FUNCTIONS

=head2 init_role

=head2 end_of_role

=head2 import

=head1 AUTHOR

Naoki Okamura (Nyarla) E<lt>nyarla[ at ]thotep.netE<gt>

=head1 SEE ALSO

L<Moose> or L<Mouse>.

L<Applish::Class::Engine>

=head1 LICENSE

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut
