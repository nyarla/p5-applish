package Applish::Logger;

use strict;
use Applish::Role;

our $CallerDepth = 0;

requires qw( log );

sub debug   { $_[0]->log( level => 'debug',     message => $_[1] ) }
sub info    { $_[0]->log( level => 'info',      message => $_[1] ) }
sub notice  { $_[0]->log( level => 'notice',    message => $_[1] ) }
sub warn    { $_[0]->log( level => 'warn',      message => $_[1] ) }
sub error   { $_[0]->log( level => 'error',     message => $_[1] ) }

__END_OF_ROLE__;

=head1 NAME

Applish::Logger - Logger interface for Applish.

=head1 SYNPOSIS

    package MyLogger;
    
    use strict;
    use Applish::Class;
    
    with 'Applish::Logger';
    
    sub log {
        my ( $self, %p ) = @_;
        my $level   = delete $p{'level'};
        my $message = delete $p{'message'};
        
        # your logging code here
    }
    
    __END_OF_CLASS__;
    
    package main;
    
    my $logger = MyLogger->new;
    
    $logger->debug($message)

=head1 DESCRIPTION

This class is logger interface definition.

=head1 INTERFACE METHODS

=head2 debug

    $logger->debug($message)

This method logs debug level message.

=head2 info

    $logger->info($message)

This method logs info level message.

=head2 notice

    $logger->notice($message)

This method logs notice level message.

=head2 warn

    $logger->warn($message)

This method logs warnings level message.

=head2 error

    $logger->error($message)

This method logs error level message.

=head1 REQUIREMENT METHODS

=head2 log

    sub log {
        my ( $self, %p ) = @_;
        my $level   = delete $p{'level'};
        my $message = delete $p{'message'};
        
        # your logging code here
    }

=head1 AUTHOR

Naoki Okamura (Nyarla) E<lt>nyarla[ at ]thotep.netE<gt>

=head1 LICENSE

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut
