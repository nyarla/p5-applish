package Applish::Class::Finder;

use strict;
use warnings;

use File::Basename;
use File::Spec;
use File::Find::Rule;

use parent qw( Exporter );

our @EXPORT = qw( find_packages );

sub find_packages {
    my ( %args ) = @_;

    my $namespace   = delete $args{'namespace'}     || q{};
    
    my @modules     = ();
    my @modfiles    = ();
    my @namespace   = grep { $_ ne q{} } split m{::}, $namespace;

    for my $inc ( ( exists $INC{'blib.pm'} ) ? grep { /blib/ } @INC : @INC ) {
        my $root = File::Spec->catfile( $inc, @namespace );
        
        next if ( ! -e $root || ! -d $root );
        
        my @files = File::Find::Rule->file->name('*.pm')->in( $root );
        
        for my $file ( @files ) {
            my ( $name, $dir ) = fileparse( $file, qr{\.pm} );
            $dir = File::Spec->abs2rel( $dir, $inc );
    
            my $module = join q{::}, ( File::Spec->splitdir( File::Spec->catfile( $dir, $name ) ) );
            push @modules, $module;
            
            push @modfiles, $file;
        }
    }

    @modules = do { my %t; grep { ! $t{$_}++ } @modules };

    return @modules;
}

1;

=head1 NAME

Applish::Class::Finder - Module finder for Applish.

=head1 SYNPOSIS

    use Applish::Class::Finder qw( find_packages );
    
    my @modules = find_packages( namespace => 'Applish::Format' );

=head1 DESCRIPTION

This class provides utilities for finding modules.

=head1 FUNCTIONS

=head2 find_packages

    my @modules = find_packages( namespace => 'Applish::Format' );

This function search modules from desiganed namespace and returns list of found modules.

=head1 AUTHOR

Naoki Okamura (Nyarla) E<lt>nyarla[ at ]thotep.netE<gt>

=head1 LICENSE

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut
