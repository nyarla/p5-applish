package Applish::Class::Singleton;

use strict;
use warnings;

sub class { die "Singletonize class is not designated. Please override this method." }

sub new {
    my ( $class, @args ) = @_;
    
    my $instance = do { no strict 'refs'; no warnings 'once'; \${"${class}::__instance"} };
    return ${$instance} if ( defined ${$instance} );

    ${$instance} = $class->class->new(@args);

    return ${$instance};
}

sub instance { goto $_[0]->can('new') }

1;

=head1 NAME

Applish::Class::Singleton - Singletonize your normal class.

=head1 SYNPOSIS

    package MyApp::Container;
    
    use strict;
    use warnings;
    
    use base qw/ Applish::Class::Singleton /;
    
    sub class { 'MyApp::Context' }
    
    1;
    
    package MyApp::Context;
    
    use strict;
    use Applish::Class;
    
    # your context class code here
    
    __END_OF_CLASS__;

=head1 DESCRIPTION

This class is wrapper for singletonize of normal classes.

=head1 INTERFACE METHODS

    # singleton !
    my $singleton = MyApp::Container->new(@args);
    
    # same
    my $singleton = MyApp::Container->instance(@args);

This method returns singleton instance.

=head1 OVERRIDE METHOD

    sub class { 'Your::Class' }

This class is make instance from return value of C<class> method.

=head1 AUTHOR

Naoki Okamura (Nyarla) E<lt>nyarla[ at ]thotep.netE<gt>

=head1 LICENSE

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut
