package Applish::Class::Requires;

use strict;
use warnings;

use Applish::Class::Engine qw( any_moose load_class is_class_loaded );
use Applish::Exceptions qw( library_import_error evaluate_error );

sub import {
    my $class   = shift;
    my $caller  = caller(0);

    while ( my $module = shift @_ )  {
           $module      = any_moose($module);
        my $argument    = ( @_ > 0 && ref($_[0]) ) ? shift @_ : [];
    
        local $@;
        eval {
            load_class($module) if ( ! is_class_loaded($module) );
        };
        library_import_error error => "Failed to load module: ${module}: ${@}"
            if ( $@ );
        
        eval qq{ package ${caller}; }
            . q{ $module->import(@{ $argument }); };
        evaluate_error error => "Failed to import module: ${module}: ${@}"
            if ( $@ );
    }

    return 1;
}

sub unimport {
    my $class   = shift;
    my $caller  = caller(0);

    while ( my $module = shift @_ )  {
           $module      = any_moose($module);
        my $argument = ( @_ > 0 && ref($_[0]) ) ? shift @_ : [] ;

        local $@;
        eval qq{ package ${caller}; }
            . q{ $module->unimport(@{ $argument }); };
        evaluate_error error => "Failed to unimport module: ${module}: ${@}"
            if ( $@ );
    }

    return 1;
}

1;

=head1 NAME

Applish::Class::Requires - (Moose|Mouse) class loader for Applish.

=head1 SYNPOSIS

    package MyClass;
    
    use strict;
    use Applish::Class;
    use Applish::Class::Requires (
        'X::Types::URI' => [qw( URI )],
    );
    
    has uri => (
        is => 'rw',
        isa => URI,
    );
    
    no Applisg::Class::Requires 'X::Type::URI';
    __END_OF_CLASS__;

=head1 DESCRIPTION

This class is (Moose|Mouse) class loader for Applish.

=head1 FUNCTIONS

=head2 import

    use Applish::Class::Requires (
        '::ClassName',
        'X::ClassName' => [ @import_args ],
        ...
    );

=head2 unimport

    no Applish::Class::Requires (
        '::ClassName',
        'X::ClassName' => [ @unimport_args ],
    );

=head1 AUTHOR

Naoki Okamura (Nyarla) E<lt>nyarla[ at ]thtoep.netE<gt>

=head1 LICENSE

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

