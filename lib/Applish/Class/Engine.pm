package Applish::Class::Engine;

use strict;
use warnings;

use Applish::Exceptions qw( library_import_error );

use parent qw( Exporter );

our @EXPORT_OK = qw(
    moose_is_preferred
    mouse_is_preferred
    any_moose
    load_class
    is_class_loaded
);
our %EXPORT_TAGS = ( 'all' => [ @EXPORT_OK ] );

our $PREFERRED;

do {
    local $@;
    if ( exists $ENV{'ANY_MOOSE'} ) {
        $PREFERRED = $ENV{'ANY_MOOSE'};
        
        unless ( $PREFERRED eq 'Moose' || $PREFERRED eq 'Mouse' ) {
            warn q{$ENV{'ANY_MOOSE'} is not set to Moose or Mouse.};
        }

        if ( $PREFERRED eq 'Moose' && ! eval { require Moose } ) {
            library_import_error error => q{$ENV{'ANY_MOOSE'} is set to 'Moose', but cannot load Moose.};
        }
        elsif ( $PREFERRED eq 'Mouse' && ! eval { require Mouse } ) {
            library_import_error error => q{$ENV{'ANY_MOOSE'} is set to 'Mouse', but cannot load Mouse.};
        }
    }
    else {
        if ( exists $INC{'Moose.pm'} ) {
            $PREFERRED = 'Moose';
        }
        elsif ( exists $INC{'Mouse.pm'} ) {
            $PREFERRED = 'Mouse';
        }
        elsif ( eval { require Mouse } ) {
            $PREFERRED = 'Mouse';
        }
        elsif ( eval { require Moose } ) {
            $PREFERRED = 'Moose';
        }
    }

    if ( ! $PREFERRED ) {
        library_import_error error => q{Unable to locate Mouse or Moose in INC.};
    }
};

sub moose_is_preferred  { $PREFERRED eq 'Moose' }
sub mouse_is_preferred  { $PREFERRED eq 'Mouse' }

our %ClassCache = ( Moose => {}, Mouse => {} );
sub any_moose           {
    my ( $name ) = @_;

    if ( ! $name ) {
        return $PREFERRED;
    }

    if ( exists $ClassCache{$PREFERRED}{$name} ) {
        return $ClassCache{$PREFERRED}{$name}
    }

    my $class = $name;
       $class =~ s{^X::}{MooseX::};
       $class =~ s{^::}{Moose::};
       $class =~ s{^Mouse(X?)\b}{Moose$1};
       $class =~ s{^(?!Moose)}{Moose::};

    if ( &mouse_is_preferred ) {
        $class =~ s{^Moose}{Mouse};
    }

    $ClassCache{$PREFERRED}{$name} = $class;

    return $class;
}

for my $method (qw( load_class is_class_loaded )) {
    no strict 'refs';
    *{__PACKAGE__ . "::${method}"} = ( moose_is_preferred() )
                                   ? *{"Class::MOP::${method}"}
                                   : *{"Mouse::Util::${method}"}
                                   ;
}

1;

=head1 NAME

Applish::Class::Engine - Class utility for switch L<Moose> or L<Mouse>.

=head SYNPOSIS

    use Applish::Class::Engine qw( :all );
    
    print any_moose('::Util')

=head1 DESCRIPTION

This class is alternative L<Any::Moose>.

This class is based L<Any::Moose> source code.

=head1 FUNCTIONS

=head2 any_moose

    my $class = any_moose('::Object') # Moose::Object or Mouse::Object

This function makes (Moose|Mouse) class name.

=head2 moose_is_perferred

This function return true if Moose is perferred.

=head2 mouse_is_perferred

This function return true if Mouse is perferred.

=head2 load_class

    load_class($class);

This method is alias L<Class::MOP::load_class> or L<Mouse::Util::load_class>.

=head2 is_class_loaded

    my $bool = is_class_loaded($class);

This method is alias L<Class::MOP::is_class_loaded> or L<Mouse::Util::is_class_loaded>.

=head1 AUTHOR

Naoki Okamura (Nyarla) E<lt>nyarla[ at ]thotep.netE<gt>

Original coded by L<Any::Moose> team.

=head1 SEE ALSO

L<Moose>, L<Mouse>

L<Any::Moose>

=head1 LICENSE

The license of this module is based on a license of L<Any::Moose>.
