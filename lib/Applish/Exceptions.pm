package Applish::Exceptions;

use strict;
use warnings;

use parent qw( Exporter );

our ( %E );
our ( @EXPORT_OK, %EXPORT_TAGS );

BEGIN {
    %E = (
        'Applish::Exception'                        => {
            description => 'Base class for Applish exceptions',
            alias       => 'exception',
        },

        'Applish::Exception::ImportError'           => {
            isa         => 'Applish::Exception',
            description => 'Failed to load something.',
            fields      => [qw( require )],
            alias       => 'import_error',
        },
        'Applish::Exception::LibraryImportError'    => {
            isa         => 'Applish::Exception::ImportError',
            description => 'Failed to load library',
            alias       => 'library_import_error',
        },

        'Applish::Exception::EvaluateError'         => {
            isa         => 'Applish::Exception',
            description => 'Evaluate is failed',
            alias       => 'evaluate_error',
        },

        'Applish::Exception::ParameterMissing'      => {
            isa         => 'Applish::Exception',
            description => 'Required parameter is missing',
            alias       => 'parameter_missing',
        },
        'Applish::Exception::ArgumentParameterMissing' => {
            isa         => 'Applish::Exception::ParameterMissing',
            description => 'Argument parameter is missing',
            alias       => 'argument_parameter_missing',
        },

        'Applish::Exception::InvalidParameter'      => {
            isa         => 'Applish::Exception',
            description => 'Parameter is invalid',
            alias       => 'invalid_parameter',
        },
        'Applish::Exception::InvalidArgumentParameter'   => {
            isa         => 'Applish::Exception::InvalidParameter',
            description => 'Argument paramter is invalid',
            alias       => 'invalid_argument_parameter',
        },
        
        'Applish::Exception::NotSupported'          => {
            isa         => 'Applish::Exception',
            description => 'This action is not supported',
            alias       => 'not_supported',
        },
    );

    for my $define ( values %E ) {
        if ( exists $define->{'alias'} ) {
            push @EXPORT_OK, $define->{'alias'};
        }
    }

    %EXPORT_TAGS = ( all => [ @EXPORT_OK ] );
}

use Exception::Class %E;

1;

=head1 NAME

Applish::Exceptions - Exception classes for Applish

=head1 SYNPOSIS

    use Applish::Exceptions;
    
    Applish::Exception->throw( error => "test exception!" )

=head1 DESCRIPTION

This class provides exception classes.

=head1 EXCEPTIONS

=head2 Applish::Exception C<exception>

Base class for exception classes

=head2 Applish::Exception::ImportError C<import_error>

Failed to load something.

=head2 Applish::Exception::LibraryImportError C<library_import_error>

Failed to load library.

=head2 Applish::Exception::EvaluateError C<evaluate_error>

Evaluate is failed.

=head2 Applish::Exception::ParameterMissing C<parameter_missing>

Required parameter is missing.

=head2 Applish::Exception::ArgumentParameterMissing C<argument_parameter_missing>.

Argument parameter is missing.

=head2 Applish::Exception::InvalidParameter C<invalid_parameter>

Parameter is invalid.

=head2 Applish::Exception::InvalidArgumentParameter C<invalid_argument_parameter>

Argument parameter is invalid.

=head2 Applish::Exception::NotSupported C<not_supported>

This action is not supported.

=head1 AUTHOR

Naoki Okamura (Nyarla) E<lt>nyarla[ at ]thotep.netE<gt>

=head1 SEE ALSO

L<Exception::Class>

=head1 LICENSE

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut
