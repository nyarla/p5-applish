package Applish::Template::Xslate;

use strict;
use Applish::Class;

with 'Applish::Template';

use Text::Xslate;
use File::stat ();

has config  => (
    is          => 'rw',
    isa         => 'HashRef',
    required    => 1,
);

has xslate  => (
    is          => 'rw',
    isa         => 'Text::Xslate',
    lazy_build  => 1,
);

sub _build_xslate {
    return Text::Xslate->new( %{ $_[0]->config } );
}

around BUILDARGS => sub {
    my $orig    = shift;
    my $class   = shift;
    my @args    = @_;

    $class->$orig( config => { @args } );
};

sub is_virtual_file {
    my ( $self ) = @_;
    return ( ref $self->config->{'path'} eq 'HASH' ) ? 1 : 0 ;
}

sub find {
    my ( $self, $filename ) = @_;

    if ( $self->is_virtual_file ) {
        if ( exists $self->config->{'path'}->{"${filename}.xt"} ) {
            return $filename;
        }
    }
    else {
        my $roots       = $self->config->{'path'};
        my $extension   = '.xt';
        
        for my $root ( @{ $roots } ) {
            my $path = "${root}/${filename}${extension}";
               $path =~ s{/+}{/}g;
               
            if ( -e $path && -r _ ) {
                return $path;
            }
        }
    }

    return undef;
}

sub render {
    my ( $self, $template, @vars ) = @_;
    return $self->xslate->render( "${template}.xt", { @vars } );
}

sub exists {
    my ( $self, $filename ) = @_;
    return ( defined $self->find($filename) ) ? 1 : 0 ;
}

sub stat {
    my ( $self, $filename ) = @_;
    my $fullpath = $self->find($filename);

    return undef if ( ! defined $fullpath );

    my $created = time;
    my $lastmod = time;

    if ( ! $self->is_virtual_file ) {
        my $stat = File::stat::stat($fullpath);
        $created = $stat->ctime;
        $lastmod = $stat->mtime;
    }

    return {
        fullpath        => $fullpath,
        created         => $created,
        lastmodified    => $lastmod,
    };
}

__END_OF_CLASS__;

=head1 NAME

Applish::Template::Xslate - L<Text::Xslate> template enigne.

=head1 SYNPOSIS

    use Applish::Template::Xslate;
    
    my $engine = Applish::Template::Xslate->new( path => ['.'] );
    
    print $engine->render('hello', name => 'foo');

=head1 DESCRIPTION

This class is template renderer using L<Text::Xslate>.

=head1 CONSTRUCTOR ARGUMENTS

    my $engine = Applish::Template::Xslate->new( %text_xslate_args )

You can designate L<Text::Xslate>'s constructor arguments to this class constructor.

Please see L<Text::Xslate> document about Text::Xslate's constructor arguments.

=head1 AUTHOR

Naoki Okamura (Nyarla) E<lt>nyarla[ at ]thotep.netE<gt>

=head1 SEE ALSO

L<Text::Xslate>

=head1 LICENSE

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut
