package Applish::Template::MicroTemplate;

use strict;
use Applish::Class;
use Text::MicroTemplate::Extended;
use File::stat ();

with 'Applish::Template';

has config => (
    is          => 'rw',
    isa         => 'HashRef',
    required    => 1,
);

has tmte => (
    is          => 'ro',
    isa         => 'Text::MicroTemplate::Extended',
    lazy_build  => 1,
);

sub _build_tmte {
    return Text::MicroTemplate::Extended->new( %{ $_[0]->config } );
}

around BUILDARGS => sub {
    my $orig    = shift;
    my $class   = shift;
    my @args    = @_;
    
    $class->$orig( config => { @args } );
};

sub find {
    my ( $self, $filename ) = @_;

    my $roots   = $self->tmte->{'include_path'};
    my $ext     = $self->tmte->{'extension'};

    for my $root ( @{ $roots } ) {
        my $path = "${root}/${filename}${ext}";
           $path =~ s{/+}{/}g;
        if ( -e $path && -r $path ) {
            return $path;
        }
    }

    return undef;
}

sub render {
    my ( $self, $path, @vars ) = @_;
    return $self->tmte->render_file( $path, @vars );
}

sub exists {
    my ( $self, $path ) = @_;
    return !! 1 if ( defined $self->find($path) );
    return !! 0;
}

sub stat {
    my ( $self, $path ) = @_;
    my $fullpath = $self->find($path);

    return undef if ( ! defined $fullpath );

    my $stat = File::stat::stat($fullpath);

    return {
        'fullpath'      => $fullpath,
        'created'       => $stat->ctime,
        'lastmodified'  => $stat->mtime,
    };
}

__END_OF_CLASS__;

=head1 NAME

Applish::Template::MicroTemplate - L<Text::MicroTemplate::Extended> support for Applish.

=head1 SYNPOSIS

    use Applish::Template::MicroTemplate;
    
    my $tmpl = Applish::Template::MicroTemplate->new( include_path => [.] );

    print $tmpl->render('hello', name => 'foo');

=head1 DESCRIPTION

This class is L<Text::MicroTemplate::Extended> support for Applish.

=head1 CONSTRUCTOR ARGUMENTS

    my $tmpl = Applish::Template::MicroTemplate->new( %microtemplate_exetnded_args );

You can designate L<Text::MicroTemplate::Extended>'s constructor arguments to constructor of this class.

Please see L<Text::MicroTemplate::Extended> document about arguments of that class.

=head1 AUTHOR

Naoki Okamura (Nyarla) E<lt>nyarla[ at ]thotep.netE<gt>

=head1 SEE ALSO

L<Text::MicroTemplate>, L<Text::MicroTemplate::File>, L<Text::MicroTemplate::Extended>

=head1 LICENSE

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut
