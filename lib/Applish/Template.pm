package Applish::Template;

use strict;
use Applish::Role;

requires qw( render exists stat );

sub flavour {
    my ( $self, $path, @vars ) = @_;

    $path =~ s{/+}{/}g;
    $path =~ s{^/*}{};
    $path =~ s{/*$}{};

    my @path        = split m{/+}, $path;
    my $filename    = pop @path;
    
    do {
        my $fullpath = join q{/}, ( @path, $filename );
        
        if ( $self->exists($fullpath) ) { 
            return $self->render($fullpath, @vars);
        }
    }
    while ( pop @path );
    
    return undef;
}

__END_OF_ROLE__;

=head1 NAME

Applish::Template - Interface definition for Template classes

=head1 SYNPOSIS

    package MyTemplate;
    
    use strict;
    use Applish::Class;
    
    with 'Applish::Template';
    
    sub render  {}
    sub exists  {}
    sub stat    {}
    
    __END_OF_CLASS__;

=head1 DESCRIPTION

This role is interface definition for template classes.

=head1 INTERFACE METHODS

=head2 render

    print $tmpl->render( $path, @vars )

This method renders a template.

B<Arguments:>

=over

=item C<$path>

A template path is designated.

This argument is required.

=item C<@vars>

Template variables are designated.

This argument is optional.

=back

=head2 flavour

    print $tmpl->flavour( $path, @vars );

This method renders a template like blosxom's flavour system.

B<Path Dispatch>:

    $path = '/path/to/index';
    
    search order:
        /path/to/index
        /path/index
        /index

when a template is not found, this method returns undef.

=head2 exists

    $bool = $tmpl->exists('/path/to/template')

This method returns a template exists.

=head2 stat

    my $stat = $tmpl->stat('/path/to/template')

This method returns a template status.

Return value is hash reference.
And when a template is not found, this method returns undef.

B<Return value parameter>:

=over

=item C<fullpath>

A template fullpath

=item C<created>

Created time of a template.

=item C<lastmodified>

Lastmodified time of a template.

=back

=head1 REQUIREMENT METHODS

=head2 render

=head2 exists

=head2 stat

=head1 AUTHOR

Naoki Okamura (Nyarla) E<lt>nyarla[ at ]thotep.netE<gt>

=head1 LICENSE

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut
