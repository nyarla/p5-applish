package Applish::Class;

use strict;
use warnings;

use Applish::Class::Engine qw( any_moose );
use Applish::Exceptions;

my ( $metaclass, $superclass, $engine, $exporter );
BEGIN {
    $engine     = any_moose();
    $metaclass  = "${engine}::Meta::Class";
    $superclass = "${engine}::Object";

    require "${engine}.pm";
}

sub init_class {
    my ( $target ) = @_;

    my $meta = $metaclass->initialize($target);
       $meta->superclasses( $superclass );

    no strict 'refs';
    no warnings 'redefine';

    *{"${target}::meta"} = sub { $meta };
}

sub end_of_class {
    my ( $target ) = @_;

    $target->meta->make_immutable();

    local $@;
    eval qq{
        package ${target};
        ${engine}->unimport();
    };

    Applish::Exception::EvaluateError->throw( error => "Cannot unimport Applish::Class: ${@}" )
        if ( $@ );

    no strict 'refs';
    delete ${"${target}::"}{'__END_OF_CLASS__'};

    return 1;
}

sub import {
    my $class   = shift;
    my $caller  = caller(0);

    return if ( $caller eq 'main' );

    strict->import;
    warnings->import;

    init_class($caller);

    no strict 'refs';
    *{"${caller}::__END_OF_CLASS__"} = sub {
        my $caller = caller(0);
        end_of_class($caller);
    };

    $engine->import({ into_level => 1 });
}

1;

=head1 NAME

Applish::Class - Class builder for Applish

=head1 SYNPOSIS

    package MyClass;
    
    use strict;
    use Applish::Class;
    
    has foo => ( is => 'rw' );
    
    __END_OF_CLASS__;

=head1 DESCRIPTION

This class is class builder for Applish.

This class alternate L<Any::Moose>.

=head1 FUNCTIONS

=head2 init_class

=head2 end_of_class

=head2 import

=head1 AUTHOR

Naoki Okamura (Nyarla) E<lt>nyarla[ at ]thotep.netE<gt>

=head1 SEE ALSO

L<Moose> or L<Mouse>.

L<Applish::Class::Engine>

=head1 LICENSE

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

