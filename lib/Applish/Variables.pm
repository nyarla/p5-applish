package Applish::Variables;

use strict;
use warnings;

use Applish::Exceptions ();
use Data::Rmap ();
use Hash::Merge::Simple ();
use Encode ();

sub new {
    my $class   = shift;
    my $source  = shift || {};

    if ( ref $source ne 'HASH' ) {
        Applish::Exception::InvalidArgumentParameter->throw( error => "Argument is not Hash reference: ${source}" );
    }

    return bless $source, $class;
}

sub merge {
    my ( $self, @sources ) = @_;

    %{ $self } = %{ Hash::Merge::Simple->merge( $self, @sources ) };

    return $self;
}

sub visit {
    my $self        = shift;
    my $callback    = shift or Applish::Exception::ArgumentParamenterMissing->throw( error => "Callback is not designated." );

    if ( ref $callback ne 'CODE' ) {
        Applish::Exception::InvalidArgumentParamter->throw( error => "Callback is not CODE reference: ${callback}" );
    }

    Data::Rmap::rmap { $_ = $callback->( $_ ) } $self;

    return $self;
}

sub decode {
    my $self        = shift;
    my $encoding    = shift or Applish::Exception::ArgumentParameterMissing->throw( error => "Decode encoding is not designated." );

    my $enc         = Encode::find_encoding($encoding);

    return $self->visit(sub { return $enc->decode( $_[0] ) });
}

sub encode {
    my $self        = shift;
    my $encoding    = shift or Applish::Exception::ArgumentParameterMissing->throw( error => "Encode encoding is not designated." );

    my $enc         = Encode::find_encoding($encoding);

    return $self->visit(sub { return $enc->encode( $_[0] ) });
}

sub set {
    my $self    = shift;
    my $section = shift or Applish::Exception::ArgumentParameterMissing->throw( error => "Variable section name is not designated." );
    my $new     = shift;
    $self->{$section} = $new;
}

sub get {
    my $self    = shift;
    my $section = shift or Applish::Exception::ArgumentParameterMissing->throw( error => "Variable section name is not designated." );

    $self->{$section};
}

1;
__END__

=head1 NAME

Applish::Variables - Variables hash object for Applish.

=head1 SYNPOSIS

    use Applish::Varabiles;
    
    my $vars = Applish::Variables->new;
       $vars->merge( \%sourcA, \%sourcB );
    
    print $vars->{'foo'}

=head1 DESCRIPTION

This class is variables hash object for Applish.

=head1 METHODS

=head2 new

    my $vars = Applish::Variables->new(\%hash);
    my $vars = Applish::Variables->new();

This method is constructor of Applish::Variables.

Hash reference is designated as first argument.

=head2 merge

    $vars->merge( \%hashA, \%hashB, ...\%hashN );

This method merges other hash references.

This method implemetned by L<Hash::Merge::Simple>.

=head2 visit

    $vars->visit(sub { lc $_[0] });

This method visits variable hash tree.

CODE reference is designated as first argument.

This method is implemetned by L<Data::Rmap>.

=head2 decode

    $vars->decode('utf8');

This method decodes value of variables hash tree.

=head2 encode

    $vars->encode('utf8');

This method encodes value of variables hash tree.

=head2 set

    $vars->set('foo' => 'bar');

This method sets new value.

=head2 get

    my $foo = $vars->get('foo')

This method gets vars value.

=head1 AUTHOR

Naoki Okamura (Nyarla) E<lt>nyarla[ at ]thtoep.netE<gt>

=head1 SEE ALSO

L<Hash::Merge::Simple>

L<Data::Rmap>

L<Encode>

=head1 LICENSE

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

