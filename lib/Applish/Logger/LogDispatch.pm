package Applish::Logger::LogDispatch;

use strict;
use Applish::Class;
use Applish::Class::Engine;
use Applish::Exceptions ();
use Applish::Types qw( LogFormatter );
use Scalar::Util ();
use Log::Dispatch;

with 'Applish::Logger';

has dispatchers => (
    is      => 'ro',
    isa     => 'ArrayRef[Str]',
    default => sub { +[qw( null )] },
);

has loggers => (
    is      => 'rw',
    isa     => 'HashRef[HashRef|Log::Dispatch::Output]',
    default => sub { +{ null => { class => 'Log::Dispatch::Null', min_level => 'error' } } },
);

has format => (
    is          => 'rw',
    isa         => LogFormatter,
    coerce      => 1,
);

has logger => (
    is          => 'rw',
    isa         => 'Log::Dispatch',
    lazy_build  => 1,
    init_arg    => undef,
);

sub _build_logger {
    my ( $self ) = @_;
    my $log = Log::Dispatch->new;

    for my $name ( @{ $self->dispatchers } ) {
        my $config = $self->loggers->{$name};

        Applish::Exception::ParameterMissing->throw( error => "dispatcher name (${name}) is not defined." )
            if ( ! defined $config );

        if ( Scalar::Util::blessed($config) ) {
            $log->add( $config );
        }
        else {
            my %config = %{ $config };
            my $class  = delete $config{'class'}
                or Applish::Exception::ParameterMissing->throw( error => "dispatcher name (${name}) is not defined dispatcher class name." );
            my %args   = %config;

            Applish::Class::Engine::load_class($class)
                if ( ! Applish::Class::Engine::is_class_loaded($class) );
            
            $log->add( $class->new( name => $name, %args ) );
        }
    }

    $log->add_callback( $self->format );

    return $log;
}

sub log {
    my ( $self, %p ) = @_;
    my $caller = caller(0);

    my $level   = delete $p{'level'};
    my $message = delete $p{'message'};

    my $depth   = 1;
       $depth++ if ( $caller eq 'Applish::Logger' );
    local $Applish::Logger::CallerDepth = $depth;

    $self->logger->log( level => $level, message => $message );
}

around BUILDARGS => sub {
    my ( $orig, $class, %args ) = @_;

    my %param;
       $param{'dispatchers'}    = delete $args{'dispatchers'};
       $param{'format'}         = delete $args{'format'};
       $param{'loggers'}        = { %args };

    return $class->$orig( %param );
};

__END_OF_CLASS__;

=head1 NAME

Applish::Logger::LogDispatch - Log::Dispatch logger for Applish.

=head1 SYNPOSIS

    use Applish::Logger::LogDispatch;
    
    my $logger = Applish::Logger::LogDispatch->new(
        dispatchers => [qw( file screen )],
        format      => q'[{date}] [{package}] {message} at {file} line {line}',
        file        => {
            class   => 'Log::Dispatch::File',
            %file_logger_args,
        },
        screen      => {
            class   => 'Log::Dispatch::Screen',
            %screen_logger_args,
        },
    );
    
    $logger->debug($message);

=head1 DESCRIPTION

This logger is L<Log::Dispatch> logger.

=head1 CONSTRUCTOR ARGUMENTS

    my $logger = Applish::Logger::LogDispatch->new(
        format      => q"[{level} {message}]" || sub { my ( %p ) = @_; },
        dispatchers => [qw( screen file )],
        screen      => {
            class       => 'Log::Dispatch::Screen',
            min_level   => 'debug',
        },
        file        => Log::Dispatch::File->new( %args ),
    );

B<Arguments>:

=over

=item C<format>

The format of the log message is designated.

The log format is designated by the form as C<{name}>.

The variable which can be designated as C<{name}> see L<Aplish::Types>'s C<LogFormatter> section.

=item C<dispatchers>

The name of logger is designated.

This argument is required.

=item Argument of the same name as logger designated as argument 'dispatcher'

Details of logger are designated by this argument.

The value which can be designated as this argument:

=over

=item instance of sub-class in L<Log::Dispatch::Output>

An instance in a sub-class in L<Log::Dispatch::Output> is designated.

=item configuration hash reference of sub-class in L<Log::Dispatch::Output>

Hash reference including several setting is designated.

Hash reference values:

=over

=item C<class>

A sub-class in L<Log::Dispatch::Output> is designated.

examples:

    Log::Dispatch::File
    Log::Dispatch::Screen
    ...etc

=item The argument which can be designated in a sub-class in L<Log::Dispatch::Output>.

The argument which can be designated in a sub-class in Log::Dispatch::Output can be designated.

The value of argument dispatcher which designates hash reference including this setting is used
for the value of argument name.

=back

=back

=back

=head1 AUTHOR

Naoki Okamura (Nyarla) E<lt>nyarla[ at ]thotep.netE<gt>

=head1 LICENSE

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut
