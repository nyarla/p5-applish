package Applish::Logger::Null;

use strict;
use Applish::Class;

with 'Applish::Logger';

sub log { 1 }

__END_OF_CLASS__;

=head1 NAME

Applish::Logger::Null - Null logger for Applish.

=head1 SYNPOSIS

    use Applish::Logger::Null;
    
    my $logger = Applish::Logger::Null->new;
       $logger->debug($message)

=head1 AUTHOR

Naoki Okamura (Nyarla) E<lt>nyarla[ at ]thotep.netE<gt>

=head1 LICENSE

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut
