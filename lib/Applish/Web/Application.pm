package Applish::Web::Application;

use strict;
use Applish::Role;

requires qw( to_app );

__END_OF_ROLE__;

=head1 NAME

Applish::Web::Application - Interface definition for web application

=head1 SYNPOSIS

    package MyApp;
    
    use strict;
    use Applish::Class;
    
    with 'Applish::Web::Application';
    
    sub to_app {
        my ( $self ) = @_;
        return sub {
            my $env = shift;
            return [ 200, [ 'Content-Type' => 'text/plain' ], [ 'hello world' ] ];
        };
    }

=head1 DESCRIPTION

This role is interface definition for PSGI applications.

=head1 INTERFACE METHOD

=head2 to_app

    my $app = MyApp->new->to_app();

This method returns L<PSGI> application handler.

=head1 REQUIREMENT METHODS

    sub to_app {
        my ( $self ) = @_;
        # psgi handler
        return sub {
            # psgi environment
            my $env = shift;
            
            # return psgi response
            return [ $status, $header, $contents ]
        };
        
    }

This method requires to return PSGI handler.

=head1 AUTHOR

Naoki Okamura (Nayrla) E<lt>nyarla[ at ]thotep.netE<gt>

=head1 SEE ALSO

L<PSGI>

L<Plack>

L<Moose>, L<Mouse>

=head1 LICENSE

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut
