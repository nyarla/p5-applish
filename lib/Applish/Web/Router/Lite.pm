package Applish::Web::Router::Lite;

use strict;
use warnings;

use parent qw( Exporter );

use Router::Simple;

our @EXPORT = qw(
    router match
    any action
    HEAD GET POST PUT DELETE
);

sub router {
    my ( $class ) = @_;

    no strict 'refs';
    no warnings 'once';
    
    ${"${class}::ROUTER"} ||= Router::Simple->new;
}

sub match           { $_[0]->router->match($_[1]) }

sub action (&)      { return $_[0] }

sub any ($$;$)      {
    my $pkg = caller(0);

    if ( @_ == 3 ) {
        my ( $methods, $pattern, $action ) = @_;
        $pkg->router->connect(
            $pattern,
            { code      => $action },
            { method    => [ map { uc $_ } @{ $methods } ] },
        );
    }
    else {
        my ( $pattern, $action ) = @_;
        $pkg->router->connect(
            $pattern,
            { code      => $action },
        );
    }
}

sub HEAD    ($$)    { caller(0)->router->connect( $_[0], { code => $_[1] }, { method => [qw/HEAD/]     } ) }
sub GET     ($$)    { caller(0)->router->connect( $_[0], { code => $_[1] }, { method => [qw/GET HEAD/] } ) }
sub POST    ($$)    { caller(0)->router->connect( $_[0], { code => $_[1] }, { method => [qw/POST/]     } ) }
sub PUT     ($$)    { caller(0)->router->connect( $_[0], { code => $_[1] }, { method => [qw/PUT/]      } ) }
sub DELETE  ($$)    { caller(0)->router->connect( $_[0], { code => $_[1] }, { method => [qw/DELETE/]   } ) }

1;
__END__

=head1 NAME

Applish::Web::Router::Lite - Writing web router using simply DSL

=head1 SYNPOSIS

    package MyRouter;
    
    use strict;
    use warnings;
    
    use Applish::Web::Router::Lite;
    
    GET '/' => action {
        return [ 200, [], [ 'hello world' ] ];
    };
    
    any [qw/ POST PUT /] => '/post' => action {
        return [ 200, [], [ 'ok' ] ];
    };

    1;

=head1 DESCRIPTION

This class provides DSL for writing web routing.

=head1 SYNTAX

=head2 HEAD

    HEAD '/path' => action {
        # your code here
    };

=head1 GET

    GET '/path' => action {
        # your code here
    };

=head2 POST

    POST '/path' => action {
        # your code here
    };

=head2 PUT

    PUT '/path' => action {
        # your code here
    };

=head2 DELETE

    DELETE '/path' => action {
        # your code here
    };

=head2 any

    any '/any' => action {
        # your code here
    };
    
    # or
    
    any [qw/ GET HEAD /] => '/get' => action {
        # your code here
    };

=head1 EXPORT CLASS METHODS

=head2 router

    my $router = MyRouter->router; # get Router::Simple instance

This method returns L<Router::Simple> instance.

=head2 match

    my $result = MyRouter->match($psgienv);

This method alias of C<MyRouter-E<gt>router-E<gt>match($psgienv)>.

=head1 AUTHOR

Naoki Okamura (Nyarla) E<lt>nyarla[ at ]thotep.netE<gt>

=head1 SEE ALSO

L<Router::Simple>

=head1 LICENSE

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut
