package Applish::Web::Server;

use strict;
use Applish::Class;
use Applish::Class::Engine;

has environ => (
    is          => 'rw',
    isa         => 'HashRef',
    required    => 1,
);

has request_class => (
    is      => 'ro',
    isa     => 'ClassName',
    default => 'Plack::Request',
);

has request => (
    is          => 'rw',
    isa         => 'Object',
    lazy_build  => 1,
);

sub _build_request {
    my ( $self ) = @_;
    my $class = $self->request_class;

    Applish::Class::Engine::load_class($class)
        if ( ! Applish::Class::Engine::is_class_loaded($class) );

    return $class->new( $self->environ );
}

has response_class => (
    is      => 'ro',
    isa     => 'ClassName',
    default => 'Plack::Response',
);

has response => (
    is          => 'rw',
    isa         => 'Object',
    lazy_build  => 1,
);

sub _build_response {
    my ( $self ) = @_;
    my $class = $self->response_class;

    Applish::Class::Engine::load_class($class)
        if ( ! Applish::Class::Engine::is_class_loaded($class) );

    return $class->new;
}

__END_OF_CLASS__;

=head1 NAME

Applish::Web::Server - PSGI enviroment class for Applish.

=head1 SYNPOSIS

    use Applish::Web::Server;
    
    my $app = sub {
        my $env     = shift;
        my $server  = Applish::Web::Server->new( environ => $env );
        
        $server->response->status(200);
        $server->response->body('hello world!');
        
        return $server->response->finalize;
    };

=head1 DESCRIPTION

This class is a PSGI environment container for Applish.

=head1 METHODS

=head2 new

    my $server = Applish::Web::Server->new( environ => $env );

This method is constructor of Applish::Web::Server.

B<Arguments>:

=over

=item C<environ>

A PASGI environment hash is designated.

This argument is required.

=item C<request_class>

A Request class name is designated.

A Request class is required L<Plack::Request> compatible.

Default value of this argument is L<Plack::Request>.

This argument is optional.

=item C<response_class>

A Response class name is designated.

A Response class is required L<Plack::Response> compatible.

Default value of this argument is L<Plack::Response>.

This argument is optional.

=back

=head2 environ

    my $env = $server->environ;

This property is accessor of PSGI environment hash reference.

=head2 request

    my $req = $server->request;

This property is instance of request class.

=head2 response

    my $res = $server->response;

This property is instance of response class.

=head2 request_class

This property holds request class name.

=head2 response_class

This property holds response class name.

=head1 AUTHOR

Naoki Okamura (Nyarla) E<lt>nyarla[ at ]thotep.netE<gt>

=head1 SEE ALSO

L<PSGI>

L<Plack>, L<Plack::Request> and L<Plack::Response>

=head1 LICENSE

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut
