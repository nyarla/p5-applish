package Applish::Web::Router;

use strict;
use Applish::Role;
use Applish::Exceptions;

requires qw( match );

__END_OF_ROLE__;

=head1 NAME

Applish::Web::Router - Interface definition for web router classes.

=head1 SYNPOSIS

    package MyRouter;
    
    use strict;
    use Applish::Class;
    
    with 'Applish::Web::Router';
    
    sub match {
        my ( $class, $psgi_env ) = @_;
        
        # your code here
        
        return {
            controller  => $controller_class,
            action      => $method,
            $param      => $value,
        }
    }
    
    __END_OF_CLASS__;

=head1 DESCRIPTION

This role is interface definition for web router classes.

=head1 INTERFACE METHODS

=head2 match

    my $matched = $router->match($psgi_env)

This class method returns matching result of web routing.

Argument is PSGI environment hash reference.

Return value is HASH reference.

=head1 REQUIREMENT METHODS

=head2 match

    sub match {
        my ( $class, $psgienv ) = @_;
        
        # your code here
        
        return { controller => $class, action => $method }
            or { code => \&action };
    };

This method must return HASH reference include controler-action or code.

=head1 AUTHOR

Naoki Okamura (Nyarla) E<lt>nyarla[ at ]thotep.netE<gt>

=head1 SEE ALSO

L<PSGI>

=head1 LICENSE

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut
