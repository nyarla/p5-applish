package Applish::Config;

use strict;
use Applish::Role;
use Applish::Types;
use Applish::Variables;
use Applish::Exceptions;

requires qw( load_config );

has vars => (
    is          => 'rw',
    isa         => 'Applish::Types::Variables',
    coerce      => 1,
    lazy_build  => 1,
);

sub _build_vars { return $_[0]->load_config }

sub section {
    my $self    = shift;
    my $section = shift or Applish::Exception::ArgumentParameterMissing->throw( error => "Configuration section name is not designated." );

    if ( @_ > 0 ) {
        $self->vars->set( $section => shift @_ );
    }

    return $self->vars->get($section);
}

sub reload { $_[0]->vars( $_[0]->load_config ); $_[0] }

__END_OF_ROLE__;

=head1 NAME

Applish::Config - Interface definition for configuration data object.

=head1 SYNPOSIS

    package MyConfig;
    
    use strict;
    use Applish::Class;
    
    with 'Applish::Config';
    
    sub load_config {
        # your code here
    }
    
    __END_OF_CLASS__;

=head1 DESCRIPTION

This role is interface definition for Configuration data objects.

=head1 INTERFACE METHODS

=head2 vars

    my $vars = $config->vars;

This method accessor of configuration data.

This method returns L<Applish::Variables> object.

=head2 section

    # get
    my $data = $config->section('foo')
    
    # set
    $config->section('foo' => $new_section_data)

This method sets or gets configuration section data.

=head2 reload
    
    $config->reload

This method reloads configuration data.

=head1 REQUIREMENT METHODS

=head2 load_config

    sub load_config {
        my ( $self ) = @_;
        # your code here
        
        return $config_hash
    }

This method should return Hash reference.

=head1 AUTHOR

Naoki Okamura (Nyarla) E<lt>nyarla[ at ]thotep.netE<gt>

=head1 LICENSE

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

