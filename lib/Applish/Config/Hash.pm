package Applish::Config::Hash;

use strict;
use Applish::Class;

with 'Applish::Config';

has source => (
    is          => 'rw',
    isa         => 'HashRef',
    required    => 1,
);

sub load_config { return $_[0]->source }

around BUILDARGS => sub {
    my $orig    = shift;
    my $class   = shift;
    my @args    = @_;

    return $class->$orig( source => { @args } );
};

__END_OF_CLASS__;

=head1 NAME

Applish::Config::Hash - Configuration object using HASH.

=head1 SYNPOSIS

    use Applish::Config::Hash;
    
    my $config = Applish::Config::Hash->new( foo => 'AAA', bar => 'BBB' )
    
    print $config->section('foo') # AAA

=head1 DESCRIPTION

This class is configuration data object using hash.

=head1 CONSTRUCTOR ARGUMENTS

    my $config = Applish::Config::Hash->new( foo => 'AAA', bar => 'BBB' )

Arguments of this class constructor is plain hash.

=head1 AUTHOR

Naoki Okamura (Nayrla) E<lt>nyarla[ at ]thotep.netE<gt>

=head1 SEE ALSO

L<Applish::Config>

=head1 LICENSE

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut
