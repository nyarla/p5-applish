package Applish::Types;

use strict;
use Applish::Class;
use Applish::Class::Engine qw( any_moose );
use Applish::Class::Requires (
    'X::Types'                  => [ -declare => [qw(
        Variables
        LogFormatter
    )] ],
    'X::Types::' . any_moose()  => [qw( Undef Str HashRef CodeRef )],
);

use Time::Piece;

my @classes = qw[
    Applish::Variables
];

for my $class ( @classes ) {
    class_type($class) if ( ! find_type_constraint($class) );
}

subtype Variables,
    as 'Applish::Variables',
;

coerce Variables,
    from HashRef,
        via { Applish::Variables->new($_) },
;

subtype LogFormatter,
    as CodeRef,
;

coerce LogFormatter,
    from Undef,
        via {
            return sub {
                my ( %p ) = @_;
                my $level   = $p{'level'};
                my $message = $p{'message'};
                
                return "[${level}] ${message}";
            };
        },
    from Str,
        via {
            my $format      = $_;
            my $need_caller = $format =~ m/[{]package|line|file[}]/;
            return sub {
                my ( %p ) = @_;

                if ( $need_caller ) {
                    my $depth = 0;
                       $depth++ while ( caller($depth) =~ m{^Log::Dispatch} );
                       $depth += $Applish::Logger::CallerDepth;
                    @p{qw( package file line )}
                        = caller($depth);
                }

                my $log = $format;
                   $log =~ s/[{]date[(](.+?)[)][}]/
                        my $fmt = $1;
                        localtime->strftime($fmt);
                   /egx;
                   $log =~ s/[{](.+?)[}]/
                        my $tag = $1;
                        if ( $tag eq 'date' ) {
                            scalar localtime;
                        }
                        elsif ( exists $p{$tag} ) {
                            $p{$tag};
                        }
                        else {
                            q{};
                        }
                   /egx;

                return $log;
            };
        },
;

__END_OF_CLASS__;

=head1 NAME

Applish::Types - (Moose|Mouse) types for Applish.

=head1 SYNPOSIS

    use MyClass;
    
    use strict;
    use Applish::Class;
    use Applish::Types qw( Cache );
    
    has cache => (
        is      => 'rw',
        isa     => Cache,
        coerce  => 1,
    );
    
    __END_OF_CLASS__;

=head1 DESCRIPTION

This package is (Moose|Mouse) types folder for Applish.

=head1 TYPES

=head2 Variables

This type is L<Applish::Variables> object type.

coerce:

=over

=item C<HashRef>

When HashRef was passed as this coerce rule.
this coerce rule make L<Applish::Variables> instance from HashRef.

=back

=head2 LogFormatter

This types is log message formatter type.

coerce:

=over

=item C<Undef>

When Undef was passed as this coerce rule,
this coerce rule returns default formatter.

=item C<Str>

When Str was passed as this coerce rule,
this coerce rule compile format string.

The format tag which can be used for a log message is as follows:

=over

=item C<level>

log message level

=item C<message>

log message

=item C<package>

package name where logger was called

=item C<file>

filename where logger was called

=item C<line>

line of file where logger was called

=item C<date>

datetime when logger was called

=item C<date($format)>

formatted datetime when logger was called.

Please see L<Time::Piece> document about how to format datetime.

=back

=back

=head1 AUTHOR

Naoki Okamura (Nayrla) E<lt>nyarla[ at ]thotep.betE<gt>

=head1 LICENSE

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut
