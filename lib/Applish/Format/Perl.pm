package Applish::Format::Perl;

use strict;
use Applish::Class;
use Applish::Exceptions;
use Data::Dumper ();

with 'Applish::Format';

sub Load {
    my ( $self, $source ) = @_;

    local $@;
    my $data = eval $source;
    
    Applish::Exception::EvaluateError->throw( error => "Failed to load perl data form text: ${@}" )
        if ( $@ );

    return $data;
}

sub Dump {
    my ( $self, $perl ) = @_;

    local $Data::Dumper::Indent     = 2;
    local $Data::Dumper::Terse      = 1;
    local $Data::Dumper::Deparse    = 1;
    local $Data::Dumper::Useqq      = 1;

    return Data::Dumper::Dumper( $perl );
}

sub extension { [qw( pl perl )] }

__END_OF_CLASS__;

=head1 NAME

Applish::Format::Perl - Perl format support for Applish

=head1 SYNPOSIS

    use Applish::Format::Perl;
    
    my $format = Applish::Format::Perl->new;
    
    my $data   = $format->Load( $perl_source );
    my $text   = $format->Dump( \$perl_structure );

=head1 DESCRIPTION

This class is perl format support for L<Applish::Format>.

=head1 AUTHOR

Naoki Okamura (Nyarla) E<lt>nyarla[ at ]thotep.netE<gt>

=head1 LICENSE

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut
