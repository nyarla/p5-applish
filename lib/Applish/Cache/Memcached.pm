package Applish::Cache::Memcached;

use strict;
use Applish::Class;
use Applish::Class::Requires (
    '::Util::TypeConstraints' => [qw( duck_type )],
);

with 'Applish::Cache';

sub engine { 'Memcached' }

has memcached => (
    is          => 'rw',
    isa         => duck_type([qw(
        namespace
        set         set_multi
        cas         cas_multi
        add         add_multi
        replace     replace_multi
        append      append_multi
        prepend     prepend_multi
        get         get_multi
        gets        gets_multi
        incr        incr_multi
        decr        decr_multi
        delete      delete_multi
        flush_all
    )]),
    required    => 1,
);

has '+namespace' => (
    trigger => sub {
        my ( $self, $ns ) = @_;
        $self->memcached->namespace($ns);
    },
);

sub set             { my $self = shift; return ( wantarray ) ? ( $self->memcached->set(@_) )            : $self->memcached->set(@_)             }
sub set_multi       { my $self = shift; return ( wantarray ) ? ( $self->memcached->set_multi(@_) )      : $self->memcached->set_multi(@_)       }
sub cas             { my $self = shift; return ( wantarray ) ? ( $self->memcached->cas(@_) )            : $self->memcached->cas(@_)             }
sub cas_multi       { my $self = shift; return ( wantarray ) ? ( $self->memcached->cas_multi(@_) )      : $self->memcached->cas_multi(@_)       }
sub add             { my $self = shift; return ( wantarray ) ? ( $self->memcached->add(@_) )            : $self->memcached->add(@_)             }
sub add_multi       { my $self = shift; return ( wantarray ) ? ( $self->memcached->add_multi(@_) )      : $self->memcached->add_multi(@_)       }
sub replace         { my $self = shift; return ( wantarray ) ? ( $self->memcached->replace(@_) )        : $self->memcached->replace(@_)         }
sub replace_multi   { my $self = shift; return ( wantarray ) ? ( $self->memcached->replace_multi(@_) )  : $self->memcached->replace_multi(@_)   }
sub append          { my $self = shift; return ( wantarray ) ? ( $self->memcached->append(@_) )         : $self->memcached->append(@_)          }
sub append_multi    { my $self = shift; return ( wantarray ) ? ( $self->memcached->append_multi(@_) )   : $self->memcached->append_multi(@_)    }
sub prepend         { my $self = shift; return ( wantarray ) ? ( $self->memcached->prepend(@_) )        : $self->memcached->prepend(@_)         }
sub prepend_multi   { my $self = shift; return ( wantarray ) ? ( $self->memcached->prepend_multi(@_) )  : $self->memcached->prepend_multi(@_)   }
sub get             { my $self = shift; return ( wantarray ) ? ( $self->memcached->get(@_) )            : $self->memcached->get(@_)             }
sub get_multi       { my $self = shift; return ( wantarray ) ? ( $self->memcached->get_multi(@_) )      : $self->memcached->get_multi(@_)       }
sub gets            { my $self = shift; return ( wantarray ) ? ( $self->memcached->gets(@_) )           : $self->memcached->gets(@_)            }
sub gets_multi      { my $self = shift; return ( wantarray ) ? ( $self->memcached->gets_multi(@_) )     : $self->memcached->gets_multi(@_)      }
sub incr            { my $self = shift; return ( wantarray ) ? ( $self->memcached->incr(@_) )           : $self->memcached->incr(@_)            }
sub incr_multi      { my $self = shift; return ( wantarray ) ? ( $self->memcached->incr_multi(@_) )     : $self->memcached->incr_multi(@_)      }
sub decr            { my $self = shift; return ( wantarray ) ? ( $self->memcached->decr(@_) )           : $self->memcached->decr(@_)            }
sub decr_multi      { my $self = shift; return ( wantarray ) ? ( $self->memcached->decr_multi(@_) )     : $self->memcached->decr_multi(@_)      }
sub delete          { my $self = shift; return ( wantarray ) ? ( $self->memcached->delete(@_) )         : $self->memcached->delete(@_)          }
sub delete_multi    { my $self = shift; return ( wantarray ) ? ( $self->memcached->delete_multi(@_) )   : $self->memcached->delete_multi(@_)    }
sub clear           { my $self = shift; return ( wantarray ) ? ( $self->memcached->flush_all(@_) )      : $self->memcached->flush_all(@_)       }


__END_OF_CLASS__;

=head1 NAME

Applish::Cache::Memcached - Memcached client for Applish

=head1 SYNPOSIS

    use Applish::Cache::Memcached;
    
    my $memcached   = Cache::Memcached::Fast->new({ servers => [ { address => '127.0.0.1:1211', }, ] });
    my $cache       = Applish::Cache::Memcached->new( memcached => $memcached );
    
    $cache->set( foo => 'bar' );
    $cache->get('foo');

=head1 DESCRIPTION

This class is memcached client for Applish.

=head2 CONSTRUCTOR ARGUMENTS.

    my $cache = Applish::Cache::Memcached->new(
        memcached => Cache::Memcached::Fast->new(\%args);
    );

Arguments:

=over

=item C<memcached>.

A instance of L<Cache::Memcached::Fast> compatiable is designated.

This argument is required.

=back

=head1 AUTHOR

Naoki Okamura (Nyarla) E<lt>nyarla[ at ]thotep.netE<gt>

=head2 SEE ALSO

L<Applish::Cache>

L<Cache::Memcached::Fast>

L<http://memcached.org>

=head1 LICENSE

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut
