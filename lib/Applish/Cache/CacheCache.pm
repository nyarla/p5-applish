package Applish::Cache::CacheCache;

use strict;
use Applish::Class;

with 'Applish::Cache';

sub engine { 'Cache::Cache' }

has cache => (
    is          => 'rw',
    isa         => 'Cache::Cache',
    required    => 1,
);

has '+namespace' => (
    trigger => sub {
        my ( $self, $ns ) = @_;
        $self->cache->set_namespace( $ns );
    },
);

sub __get_casid {
    my ( $self, $key ) = @_;

    my $cache   = $self->cache;
    my $ns      = $cache->get_namespace;

    $cache->set_namespace("__casid:${ns}");

    my $casid   = $cache->get($key);
       $casid   = 1 if ( ! defined $casid );
       $casid++;
    
    $cache->set( $key, $casid );
    $cache->set_namespace($ns);

    return $casid;
}


sub set {
    my ( $self, $key, $value, $expire ) = @_;

    if ( defined $expire ) {
        $expire = "${expire} sec";
    }

    my $casid = $self->__get_casid($key);
       $casid++;

    $self->cache->set( $key, { value => $value, casid => $casid }, $expire );

    return !! 1;
}

sub get {
    my ( $self, $key ) = @_;

    my $ret = $self->cache->get($key);

    if ( ref $ret eq 'HASH' ) {
        return $ret->{'value'};
    }

    return undef;
}

sub delete {
    my ( $self, $key ) = @_;
    my $cache = $self->cache;

    if ( ref $cache->get($key) ne 'HASH' ) {
        return !! 0;
    }

    $cache->remove($key);
    return !! 1;
}

sub cas {
    my ( $self, $key, $cas, $value, $expire ) = @_;

    my $cache = $self->cache->get($key);

    if ( ref $cache eq 'HASH' ) {
        my $casid = $cache->{'casid'};

        if ( $casid == $cas ) {
            return $self->set( $key, $value, $expire );
        }
    }

    return !! 0;
}

sub gets {
    my ( $self, $key ) = @_;
    my $cache = $self->cache->get($key);

    if ( ref $cache eq 'HASH' ) {
        return [ $cache->{'casid'}, $cache->{'value'} ];
    }

    return;
}

sub clear {
    my ( $self ) = @_;
    $self->clear();

    return !! 1;
}

__END_OF_CLASS__;

=head1 NAME

Applish::Cache::CacheCache - Cache::Cache family adaptor for Applish.

=head1 SYNPOSIS

    use Applish::Cache::CacheCache;
    use Cache::MemoryCache;
    
    my $cache = Applish::Cache::CacheCache->new(
        cache => Cache::MemoryCache->new(),
    );
    
    $cache->set( $key, $value, $expire );
    
    my $ret = $cache->get($key);

=head1 DESCRIPTION

This class is adaptor for L<Cache::Cache> family.

=head1 CONSTRUCTOR ARGUMENTS

    my $cache = Applish::Cache::CacheCache->new(
        cache => $CacheCaheSubClassInstance,
    );

Constructor Arguments:

=over

=item C<cache>

A instance of L<Cache::Cache> family is designated.

This argument is required.

=back

=head1 AUTHOR

Naoki Okamura (Nyarla) E<lt>nyarla[ at ]thotep.netE<gt>

=head1 SEE ALSO

L<Cache::Cache>

L<Applish::Cache>

=head1 LICENSE

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut
