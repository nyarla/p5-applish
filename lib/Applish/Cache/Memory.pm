package Applish::Cache::Memory;

use strict;
use Applish::Class;

with qw( Applish::Cache );

sub engine { 'Memory' }

has __cache => (
    is      => 'rw',
    isa     => 'HashRef',
    default => sub { +{} },
);

has __casid => (
    is      => 'rw',
    isa     => 'HashRef',
    default => sub { +{} },
);

sub __prepare_cache {
    my ( $self ) = @_;
    my $ns = $self->namespace;

    if ( ! exists $self->__cache->{$ns} ) {
        $self->__cache->{$ns} = {};
    }

    return $self->__cache->{$ns};
}

sub __prepare_casid {
    my ( $self ) = @_;
    my $ns = $self->namespace;

    if ( ! exists $self->__casid->{$ns} ) {
        $self->__casid->{$ns} = {};
    }

    return $self->__casid->{$ns};
}

sub set {
    my ( $self, $key, $value, $expire ) = @_;

    $expire = 0 if ( ! defined $expire );
    my $cache = $self->__prepare_cache;
    my $casid = $self->__prepare_casid;
    
    $casid->{$key} = 1 if ( ! exists $casid->{$key} );
    $casid->{$key}++;


    $cache->{$key} = {
        value   => $value,
        ctime   => time,
        expire  => $expire,
        casid   => $casid->{$key},
    };

    return !! 1;
}

sub get {
    my ( $self, $key ) = @_;
    my $cache = $self->__prepare_cache;

    if ( exists $cache->{$key} ) {
        my $store = $cache->{$key};

        if ( $store->{'expire'} != 0 ) {
            if ( ( $store->{'ctime'} + $store->{'expire'} ) <= time  ) {
                delete $cache->{$key};
                return;
            }
        }

        return $store->{'value'};
    }

    return;

}

sub delete {
    my ( $self, $key ) = @_;
    my $cache = $self->__prepare_cache;

    if ( exists $cache->{$key} ) {
        delete $cache->{$key};
        return !! 1;
    }

    return !! 0;
}

sub cas {
    my ( $self, $key, $cas, $value, $expire ) = @_;
    my $cache = $self->__prepare_cache;

    if ( exists $cache->{$key} ) {
        my $store = $cache->{$key};
        my $casid = $store->{'casid'};
        
        if ( $cas == $casid ) {
            $self->set( $key, $value, $expire );
            return !! 1;
        }
    }

    return !! 0;
}

sub gets {
    my ( $self, $key ) = @_;
    my $cache = $self->__prepare_cache;

    if ( exists $cache->{$key} ) {
        my $store = $cache->{$key};

        return [ $store->{'casid'}, $store->{'value'} ];
    }

    return;
}

sub clear {
    my ( $self ) = @_;

    $self->cache({});

    return !! 1;
}

__END_OF_CLASS__;

=head1 NAME

Applish::Cache::Memory - Memory cache for Applish

=head1 SYNPOSIS

    use Applish::Cache::Memory;
    
    my $cache = Applish::Cache::Memory->new();
    
    $cache->set( $key, $value, $expire );
    my $ret = $cache->get($key);

=head1 DESCRIPTION

This class is memory cache class for Applish.

=head1 AUTHOR

Naoki Okamura (Nyarla) E<lt>nyarla[ at ]thotep.netE<gt>

=head1 LICENSE

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut
