package Applish::Format;

use strict;
use Applish::Role;

requires qw[ Load Dump extension ];

sub is_supported_file {
    my ( $self, $file ) = @_;

    for my $extension ( @{ $self->extension } ) {
        if ( $file =~ m{\.$extension$} ) {
            return 1;
        }
    }

    return 0;
}

__END_OF_ROLE__;

=head1 NAME

Applish::Format - Interface definition of file formatter.

=head1 SYNPOSIS

    package MyFormat;
    
    use strict;
    use Applish::Class;
    
    with 'Applish::Format';
    
    sub Load { # your load code here }
    sub Dump { # your dump code here }
    
    sub extension { \@extensions }
    
    __END_OF_CLASS__;

=head1 DESCRIPTION

This role is interface definition of file formatter.

=head1 INTERFACE METHODS

=head2 Load

    my $data = $loader->Load( $source_text );

This method loads data from formatted text.

=head2 Dump

    my $text = $loader->Dump( \%perl_data );

This method dumps formatted text from perl data.

=head2 is_supported_file

    my $bool = $loader->is_supported_file( $path )

This method returns whether a file is supported.

=head1 REQUIREMENT METHODS

=head2 Load

    sub Load {
        my ( $source ) = @_;
        # your code here
        return $perl_data
    }

This method loads data from formatted text.

=head2 Dump

    sub Dump {
        my ( $perl_structure ) = @_;
        # your code here
        return $formatted_string;
    }

This method dumps formatted text from perl data structure.

=head2 extension

    sub exetnsion { [ pl perl ] }

This method returns ARRAY referernce include file extension.

=head1 AUTHOR

Naoki Okamura (Nyarla) E<lt>nyarla[ at ]thotep.netE<gt>

=head1 LICENSE

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut
