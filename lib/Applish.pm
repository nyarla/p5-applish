package Applish;

use strict;
use warnings;

our $VERSION = '0.00001';

1;
__END__

=head1 NAME

Applish -

=head1 SYNOPSIS

  use Applish;

=head1 DESCRIPTION

Applish is

=head1 AUTHOR

Naoki Okamrua (Nyarla) E<lt>nyarla[ at ]thotep.netE<gt>

=head1 SEE ALSO

=head1 LICENSE

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut
