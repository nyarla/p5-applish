#!perl

use strict;
use warnings;

use Test::More;
use Test::Requires qw( HTTP::Request Plack::Test Plack::Request Plack::Response );

use Applish::Web::Server;

test_psgi(
    app     => sub {
        my ( $env ) = @_;
        
        my $server = Applish::Web::Server->new(environ => $env);
        
        isa_ok( $server->request,   'Plack::Request'    );
        isa_ok( $server->response,  'Plack::Response'   );
        
        $server->response->status(200);
        $server->response->body('hello world!');
        
        return $server->response->finalize;
    },
    client  => sub {
        my ( $cb ) = @_;
        my $req = HTTP::Request->new( GET => 'http://localhost/' );
        my $res = $cb->( $req );
        
        is( $res->code, 200 );
        is( $res->content, 'hello world!' );
    },
);

done_testing;
