#!perl

use strict;
use warnings;

use Test::More;
use Applish::Formats;

my $formats = Applish::Formats->new;

is( $formats->load( Perl => q{ 'foo' } ), 'foo' );
is_deeply(
    eval $formats->dump( Perl => { foo => 'bar' } ),
    { foo => 'bar' },
);

ok( $formats->is_supported('Perl') );
ok( ! $formats->is_supported('NotFound') );

is( $formats->detect_file_format('file.pl'), 'Perl' );

is_deeply(
    $formats->detect_load( 'file.pl', q[ { foo => 'bar' } ] ),
    { foo => 'bar' },
);

done_testing;
