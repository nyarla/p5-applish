#!perl

use strict;
use warnings;

use Test::More;
use Applish::Class ();

{ package TestClass }

Applish::Class::init_class('TestClass');

my $meta = TestClass->meta;

isa_ok(
    $meta,
    Applish::Class::any_moose('::Meta::Class'),
);

is_deeply(
    [ $meta->superclasses ],
    [ Applish::Class::any_moose('::Object') ],
);

done_testing;
