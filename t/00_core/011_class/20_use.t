#!perl

use strict;
use warnings;

use Test::More;

{
    package Foo;
    
    use Applish::Class;
    
    has bar => ( is => 'rw' );
    
    __END_OF_CLASS__;
}

ok( ! Foo->can('__END_OF_CLASS__') );
ok( ! Foo->can('has') );

ok( Foo->meta->is_immutable );

my $instance = Foo->new( bar => 'baz' );

is( $instance->bar, 'baz' );

done_testing;
