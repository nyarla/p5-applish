#!perl

use strict;
use warnings;

use Test::More;

BEGIN {
    use_ok('Applish::Logger::Null');
}

my $logger = Applish::Logger::Null->new;

isa_ok( $logger, 'Applish::Logger::Null' );

ok( $logger->debug('message!') );

done_testing;
