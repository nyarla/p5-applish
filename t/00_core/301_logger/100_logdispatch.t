#!perl

use strict;
use warnings;

use Test::Requires qw( Log::Dispatch );
use Test::More;

BEGIN {
    use_ok('Applish::Logger::LogDispatch');
}

{
    package MyLogger::String;
    
    use parent qw( Log::Dispatch::Output );

    sub new {
        my ( $class, %args ) = @_;
        my $self = bless { string => $args{'string'} }, $class;
           $self->_basic_init( %args );

        return $self;
    }

    sub log_message {
        my ( $self, %p ) = @_;

        ${ $self->{'string'} } .= $p{'message'};
    }

    $INC{'MyLogger/String.pm'} = $0;
    1;
}

my $string = q{};
my $ref    = \$string;
my $logger = Applish::Logger::LogDispatch->new(
    dispatchers => [qw( string )],
    format      => q[{package}|{level}|{message}],
    string      => {
        class       => 'MyLogger::String',
        min_level   => 'debug',
        string      => $ref,
    }
);

isa_ok( $logger->logger, 'Log::Dispatch' );

$logger->debug('message!');

is( $string, q[main|debug|message!] );

${ $ref } = q{};

$logger->log( level => 'info', message => 'foo!bar!' );

is( $string, q[main|info|foo!bar!] );

done_testing;
