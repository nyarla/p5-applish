#!perl

use strict;
use warnings;

use Test::More;
use Test::Requires qw( Cache::MemoryCache );

BEGIN { use_ok('Applish::Cache::CacheCache') }

sub cache {
    Applish::Cache::CacheCache->new( cache => Cache::MemoryCache->new() );
}

isa_ok( cache(), 'Applish::Cache::CacheCache' );

subtest 'basic' => sub {
    my $cache = cache();

    ok( $cache->set( foo => 'bar' ) );
    is( $cache->get('foo'), 'bar' );

    done_testing;
};

subtest 'namespace' => sub {
    my $cache = cache();

    $cache->namespace('foo');
    
    ok( $cache->set( foo => 'bar' ) );

    $cache->namespace('');
    
    ok( ! $cache->get('foo') );
    
    $cache->namespace('foo');

    is( $cache->get('foo'), 'bar' );

    done_testing;
};

subtest 'cas' => sub {
    my $cache = cache();

    $cache->set_multi(
        [ 'foo' => 'AAA' ],
        [ 'bar' => 'BBB' ],
    );

    my $cas = $cache->gets_multi(qw( foo bar ));

    $cas->{'foo'}->[1] = 'foo';
    $cas->{'bar'}->[1] = 'bar';

    $cache->set( bar => 'DDD' );

    my @results = $cache->cas_multi(
        [ 'foo', @{ $cas->{'foo'} } ],
        [ 'bar', @{ $cas->{'bar'} } ],
    );
    
    is_deeply(
        [ @results ],
        [ !! 1, !! 0 ],
    );

    is( $cache->get('foo'), 'foo' );
    is( $cache->get('bar'), 'DDD' );

    $cas = $cache->gets_multi(qw( foo bar ));
    
    $cas->{'foo'}->[1] = 'AAA';
    $cas->{'bar'}->[1] = 'BBB';

    $cache->set( foo => 'baz' );
    
    my $results = $cache->cas_multi(
        [ foo => @{ $cas->{'foo'} } ],
        [ bar => @{ $cas->{'bar'} } ],
    );
    
    ok( ! $results->{'foo'} );
    ok( $results->{'bar'} );

    done_testing;
};

subtest 'delete' => sub {
    my $cache = cache();

    $cache->set_multi([ foo => 'AAA' ], [ bar => 'BBB' ], [ baz => 'CCC' ]);

    is_deeply(
        [ $cache->delete_multi(qw( foo )) ],
        [ !! 1 ],
    );

    my $result = $cache->delete_multi(qw( foo bar ));
    is_deeply(
        $result,
        {
            foo => !! 0,
            bar => !! 1,
        },
    );

    is_deeply(
        $cache->get_multi(qw( foo bar baz )),
        {
            baz => 'CCC',
        }
    );

    done_testing;
};

done_testing;
