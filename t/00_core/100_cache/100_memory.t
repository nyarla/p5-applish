#!perl

use strict;
use warnings;

use Test::More;

BEGIN { use_ok('Applish::Cache::Memory') }

sub cache { Applish::Cache::Memory->new() }

isa_ok( &cache, 'Applish::Cache::Memory' );

subtest 'basic' => sub {
    my $cache = cache();

    ok( $cache->set( foo => 'bar' ) );
    is( $cache->get('foo'), 'bar' );

    done_testing;
};

subtest 'basic_multi' => sub {
    my $cache = cache();
    
    my @set = ( [ foo => 'AAA' ], [ bar => 'BBB' ] );

    is_deeply(
        [ $cache->set_multi(@set) ],
        [ !! 1, !! 1 ],
    );
    
    is_deeply(
        { %{ $cache->set_multi(@set) } },
        {
            foo => !! 1,
            bar => !! 1,
        },
    );

    is_deeply(
        $cache->get_multi(qw( foo bar )),
        {
            foo => 'AAA',
            bar => 'BBB',
        }
    );

    done_testing;
};

subtest 'namespace' => sub {
    my $cache = cache();

    $cache->namespace('foo');
    
    ok( $cache->set( foo => 'bar' ) );

    $cache->namespace('');
    
    ok( ! $cache->get('foo') );
    
    $cache->namespace('foo');

    is( $cache->get('foo'), 'bar' );

    done_testing;
};

subtest 'cas' => sub {
    my $cache = cache();

    $cache->set_multi(
        [ 'foo' => 'AAA' ],
        [ 'bar' => 'BBB' ],
    );

    my $cas = $cache->gets_multi(qw( foo bar ));

    $cas->{'foo'}->[1] = 'foo';
    $cas->{'bar'}->[1] = 'bar';

    $cache->set( bar => 'DDD' );

    my @results = $cache->cas_multi(
        [ 'foo', @{ $cas->{'foo'} } ],
        [ 'bar', @{ $cas->{'bar'} } ],
    );
    
    is_deeply(
        [ @results ],
        [ !! 1, !! 0 ],
    );

    is( $cache->get('foo'), 'foo' );
    is( $cache->get('bar'), 'DDD' );

    $cas = $cache->gets_multi(qw( foo bar ));
    
    $cas->{'foo'}->[1] = 'AAA';
    $cas->{'bar'}->[1] = 'BBB';

    $cache->set( foo => 'baz' );
    
    my $results = $cache->cas_multi(
        [ foo => @{ $cas->{'foo'} } ],
        [ bar => @{ $cas->{'bar'} } ],
    );
    
    ok( ! $results->{'foo'} );
    ok( $results->{'bar'} );

    done_testing;
};

subtest 'add' => sub {
    my $cache = cache();

    my @result = $cache->add_multi(
        [ 'foo' => 'bar' ],
        [ 'bar' => 'baz' ],
    );
    
    is_deeply(
        [ @result ],
        [ !! 1, !! 1 ],
    );
    
    is_deeply(
        $cache->get_multi(qw( foo bar )),
        {
            foo => 'bar',
            bar => 'baz',
        },
    );
    
    my $result = $cache->add_multi(
        [ 'foo' => 'AAA' ],
        [ 'baz' => 'BBB' ],
    );
    
    is_deeply(
        $result,
        {
            foo => !! 0,
            baz => !! 1,
        }
    );
    
    is_deeply(
        $cache->get_multi(qw( foo baz )),
        {
            foo => 'bar',
            baz => 'BBB',
        }
    );

    done_testing;
};

subtest 'replace' => sub {
    my $cache = cache();

    $cache->set_multi(
        [ 'foo' => 'bar' ],
    );

    my @results = $cache->replace_multi(
        [ 'foo' => 'baz' ],
        [ 'bar' => 'foo' ],
    );

    is_deeply(
        [ @results ],
        [ !! 1, !! 0 ],
    );

    is_deeply(
        $cache->get_multi(qw( foo )),
        { foo => 'baz' },
    );

    my $result = $cache->replace_multi(
        [ 'foo' => 'AAA' ],
        [ 'bar' => 'BBB' ],
    );

    is_deeply(
        $result,
        {
            foo => !! 1,
            bar => !! 0,
        },
    );

    is( $cache->get('foo'), 'AAA' );

    done_testing;
};

subtest 'append' => sub {
    my $cache = cache();

    $cache->set_multi(
        [ 'foo' => 'AAA' ],
        [ 'bar' => 'BBB' ],
    );

    is_deeply(
        [ $cache->append_multi([ foo => 'BBB' ], [ 'bar' => 'CCC' ]) ],
        [ !! 1, !! 1 ],
    );

    is_deeply(
        $cache->get_multi(qw( foo bar )),
        {
            foo => 'AAABBB',
            bar => 'BBBCCC',
        }
    );
    
    my $result = $cache->append_multi([ foo => 'CCC' ], [ bar => 'DDD' ]);

    is_deeply(
        $result,
        {
            foo => !! 1,
            bar => !! 1,
        },
    );

    is_deeply(
        $cache->get_multi(qw( foo bar )),
        {
            foo => 'AAABBBCCC',
            bar => 'BBBCCCDDD',
        }
    );

    done_testing;
};

subtest 'prepend' => sub {
    my $cache = cache();

    $cache->set_multi(
        [ 'foo' => 'AAA' ],
        [ 'bar' => 'BBB' ],
    );

    is_deeply(
        [ $cache->prepend_multi([ foo => 'BBB' ], [ 'bar' => 'CCC' ]) ],
        [ !! 1, !! 1 ],
    );

    is_deeply(
        $cache->get_multi(qw( foo bar )),
        {
            foo => 'BBBAAA',
            bar => 'CCCBBB',
        }
    );
    
    my $result = $cache->prepend_multi([ foo => 'CCC' ], [ bar => 'DDD' ]);

    is_deeply(
        $result,
        {
            foo => !! 1,
            bar => !! 1,
        },
    );

    is_deeply(
        $cache->get_multi(qw( foo bar )),
        {
            foo => 'CCCBBBAAA',
            bar => 'DDDCCCBBB',
        }
    );

    done_testing;
};

subtest 'incr' => sub {
    my $cache = cache();

    $cache->set_multi([ foo => 1], [ bar => 10 ]);

    is_deeply(
        [ $cache->incr_multi(qw( foo bar )) ],
        [ 2, 11 ],
    );

    is_deeply(
        [ $cache->incr_multi([qw( foo )], [ bar => 20 ]) ],
        [ 3, 31 ],
    );

    is_deeply(
        $cache->get_multi(qw( foo bar )),
        {
            foo => 3,
            bar => 31,
        }
    );

    my $result = $cache->incr_multi( qw( foo bar ) );

    is_deeply(
        $result,
        {
            foo => 4,
            bar => 32,
        }
    );

    done_testing;
};

subtest 'decr' => sub {
    my $cache = cache();

    $cache->set_multi([ foo => 10], [ bar => 20 ]);

    is_deeply(
        [ $cache->decr_multi(qw( foo bar )) ],
        [ 9, 19 ],
    );

    is_deeply(
        [ $cache->decr_multi([qw( foo )], [ bar => 10 ]) ],
        [ 8, 9 ],
    );

    is_deeply(
        $cache->get_multi(qw( foo bar )),
        {
            foo => 8,
            bar => 9,
        }
    );

    my $result = $cache->decr_multi( qw( foo bar ) );

    is_deeply(
        $result,
        {
            foo => 7,
            bar => 8,
        }
    );

    done_testing;
};

subtest 'delete' => sub {
    my $cache = cache();

    $cache->set_multi([ foo => 'AAA' ], [ bar => 'BBB' ], [ baz => 'CCC' ]);

    is_deeply(
        [ $cache->delete_multi(qw( foo )) ],
        [ !! 1 ],
    );

    my $result = $cache->delete_multi(qw( foo bar ));
    is_deeply(
        $result,
        {
            foo => !! 0,
            bar => !! 1,
        },
    );

    is_deeply(
        $cache->get_multi(qw( foo bar baz )),
        {
            baz => 'CCC',
        }
    );

    done_testing;
};

done_testing;
