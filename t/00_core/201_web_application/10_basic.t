#!perl

use strict;
use warnings;

use Test::More;

local $@;
eval q{
    package MyApp;
    
    use strict;
    use Applish::Class;
    
    with 'Applish::Web::Application';
};

ok( $@ );

local $@;
eval q{
    package MyApp2;
    
    use strict;
    use Applish::Class;
    
    with 'Applish::Web::Application';
    
    sub to_app {}
};

ok( ! $@ );

done_testing;
