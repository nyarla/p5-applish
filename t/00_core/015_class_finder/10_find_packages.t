#!perl

use strict;
use warnings;

use Test::More;
use Applish::Class::Finder qw( find_packages );

is_deeply(
    [ sort qw( Applish::Class::Engine Applish::Class::Requires Applish::Class::Finder ) ],
    [ sort &find_packages( namespace => 'Applish::Class' ) ],
);

done_testing;
