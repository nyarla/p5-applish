#!perl

use strict;
use warnings;

use Test::More;
use Applish::Role ();

{ package TestRole }

Applish::Role::init_role('TestRole');

isa_ok(
    TestRole->meta,
    Applish::Role::any_moose('::Meta::Role'),
);

done_testing;
