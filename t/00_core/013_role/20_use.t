#!perl

use strict;
use warnings;

use Test::More;

{
    package TestRole;

    use strict;
    use Applish::Role;

    requires qw( foo );
    
    has bar => ( is => 'rw' );
    
    
    __END_OF_ROLE__;
}

ok( ! TestRole->can('__END_OF_ROLE__') );
ok( ! TestRole->can('has') );

{
    package TestClass;
    
    use strict;
    use Applish::Class;
    
    with qw( TestRole );
    
    sub foo { return 'AAA' }
}

my $instance = TestClass->new( foo => 'AAA', bar => 'BBB' );

is( $instance->foo, 'AAA' );
is( $instance->bar, 'BBB' );

done_testing;
