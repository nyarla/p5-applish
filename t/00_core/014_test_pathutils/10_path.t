#!perl

use strict;
use warnings;

use Test::More;
use Applish::Test::PathUtils qw( $rootdir $testdir $examples );

use File::Spec;
use FindBin ();

{
    my @path = File::Spec->splitdir( $FindBin::Bin );
    while ( my $path = pop @path ) {
        if ( $path eq 't' ) {
            my $root = File::Spec->catfile( @path );
            is( $rootdir, $root );
            is( $testdir, File::Spec->catfile( $root, 't' ) );
            is( $examples, File::Spec->catfile( $root, 't', 'examples' ) );
        }
    }
}

done_testing;
