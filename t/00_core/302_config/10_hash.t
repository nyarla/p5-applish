#!perl

use strict;
use warnings;

use Test::More;

BEGIN { use_ok('Applish::Config::Hash') }

my $config = Applish::Config::Hash->new( foo => 'AAA' );

isa_ok( $config, 'Applish::Config::Hash' );

is_deeply(
    $config->source,
    { foo => 'AAA' },
);

is_deeply(
    $config->vars,
    Applish::Variables->new({ foo => 'AAA' }),
);

is( $config->section('foo'), 'AAA' );

$config->section('bar' => 'BBB');

is( $config->section('bar'), 'BBB' );

$config->source({ baz => 'CCC' });
$config->reload;

is_deeply(
    $config->vars,
    Applish::Variables->new({ baz => 'CCC' }),
);

done_testing;
