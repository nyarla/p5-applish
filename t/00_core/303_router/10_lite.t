#!perl

use strict;
use warnings;

use Test::More;

BEGIN { use_ok('Applish::Web::Router::Lite') }

local $@;
eval {
    package MyRouter;
    
    use strict;
    use warnings;
    
    use Applish::Web::Router::Lite;
    
    HEAD    '/head'     => action   { 'HEAD'    };
    GET     '/get'      => action   { 'GET'     };
    POST    '/post'     => action   { 'POST'    };
    PUT     '/put'      => action   { 'PUT'     };
    DELETE  '/delete'   => action   { 'DELETE'  };
    
    any '/any'  => action { 'ANY1' };
    any [qw/ GET HEAD /] => '/gh' => action { 'ANY2' };
    
    1;
};

ok( ! $@ );

can_ok( MyRouter => qw[ router match ]);

my @tests = (
    [qw( /head HEAD HEAD        )],
    [qw( /get GET GET           )],
    [qw( /post POST POST        )],
    [qw( /put PUT PUT           )],
    [qw( /delete DELETE DELETE  )],
    [qw( /any   GET     ANY1    )],
    [qw( /gh    GET     ANY2    )],
    [qw( /gh    HEAD    ANY2    )],
);

for my $test ( @tests ) {
    my ( $path, $method, $result ) = @{ $test };

    is(
        MyRouter->match({ REQUEST_METHOD => $method, PATH_INFO => $path })->{'code'}->(),
        $result,
    );
}

ok( ! MyRouter->match({ REQUEST_METHOD => 'POST', PATH_INFO => '/gh' }) );

done_testing;
