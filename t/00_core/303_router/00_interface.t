#!perl

use strict;
use warnings;

use Test::More;

BEGIN { use_ok('Applish::Web::Router') }

local $@;
eval {
    package MyRouter;
    
    use strict;
    use Applish::Class;
    
    with 'Applish::Web::Router';
    
    sub match { return { code => sub { return [ 200, [], [] ]  } } }
    
    __END_OF_CLASS__;
};

ok( ! $@ );

ok( MyRouter->does('Applish::Web::Router') );

can_ok( MyRouter => qw[ match ] );

is_deeply(
    MyRouter->match->{'code'}->(),
    [ 200, [], [] ],
);

done_testing;
