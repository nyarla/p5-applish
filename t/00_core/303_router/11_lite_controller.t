#!perl

use strict;
use warnings;

use Test::More;

BEGIN { use_ok('Applish::Web::Router::Lite::Controller') }

local $@;
eval {
    package MyRouter;
    
    use strict;
    use warnings;
    
    use Applish::Web::Router::Lite::Controller;
    
    HEAD    '/head'     => 'Root'   => 'head';
    GET     '/get'      => 'Root'   => 'get';
    POST    '/post'     => 'Root'   => 'post';
    PUT     '/put'      => 'Root'   => 'put';
    DELETE  '/delete'   => 'Root'   => 'delete';
    
    any '/any'  => 'Any'    => 'any1';
    any [qw/ GET HEAD /] => '/gh' => 'Any' => 'any2';
    
    1;
};

ok( ! $@ );

can_ok( MyRouter => qw( match router ) );

my @tests = (
    [qw( /head      HEAD    Root    head    )],
    [qw( /get       GET     Root    get     )],
    [qw( /post      POST    Root    post    )],
    [qw( /put       PUT     Root    put     )],
    [qw( /delete    DELETE  Root    delete  )],
    [qw( /any       GET     Any     any1    )],
    [qw( /gh        GET     Any     any2    )],
    [qw( /gh        HEAD    Any     any2    )],
);

for my $test ( @tests ) {
    my ( $path, $method, $controller, $action ) = @{ $test };

    is_deeply(
        MyRouter->match({ REQUEST_METHOD => $method, PATH_INFO => $path }),
        {
            controller  => $controller,
            action      => $action,
        }
    );
}

ok( ! MyRouter->match({ REQUEST_METHOD => 'POST', PATH_INFO => '/gh' }) );

done_testing;
