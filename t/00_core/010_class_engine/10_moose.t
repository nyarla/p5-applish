#!perl

use strict;
use warnings;

use Test::More;

BEGIN {
    $ENV{'ANY_MOOSE'} = 'Moose';

    if ( $^O eq 'cygwin' && $] == 5.010001 ) {
        plan skip_all => "Moose does not work in cygwin perl 5.10.1-[2-3]";
    }
}

use Applish::Class::Engine qw( :all );

is( $Applish::Class::Engine::PREFERRED, 'Moose' );

ok( moose_is_preferred );
ok( ! mouse_is_preferred );

my %tasks = (
    '::Util'        => 'Moose::Util',
    'X::Types'      => 'MooseX::Types',
    'Mouse::Object' => 'Moose::Object',
    'Util'          => 'Moose::Util',
    ''              => 'Moose',
);

while ( my ( $source, $result ) = each %tasks ) {
    is( any_moose($source), $result );
}

load_class( any_moose('::Util') );
ok( is_class_loaded( any_moose('::Util') ) );

done_testing;
