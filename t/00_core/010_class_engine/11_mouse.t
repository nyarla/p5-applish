#!perl

use strict;
use warnings;

BEGIN {
    $ENV{'ANY_MOOSE'} = 'Mouse';
}

use Test::More;
use Applish::Class::Engine qw( :all );

is( $Applish::Class::Engine::PREFERRED, 'Mouse' );

ok( ! moose_is_preferred );
ok( mouse_is_preferred );

my %tasks = (
    '::Util'        => 'Mouse::Util',
    'X::Types'      => 'MouseX::Types',
    'Mouse::Object' => 'Mouse::Object',
    'Util'          => 'Mouse::Util',
    ''              => 'Mouse',
);

while ( my ( $source, $result ) = each %tasks ) {
    is( any_moose($source), $result );
}

load_class( any_moose('::Util') );
ok( is_class_loaded( any_moose('::Util') ) );

done_testing;
