#!perl

use strict;
use warnings;

use Test::More;
use Applish::Types qw[ Variables ];
use Applish::Variables;

my $meta = Variables;

is( "${meta}", 'Applish::Types::Variables' );

ok( $meta->check( Applish::Variables->new ) );
ok( ! $meta->check({}) );

is_deeply(
    $meta->coerce({foo => 'bar'}),
    Applish::Variables->new({ foo => 'bar' }),
);

done_testing;
