#!perl

use strict;
use warnings;

use Test::More;
use Applish::Exceptions qw( :all );

my @tasks = (
    {
        class   => 'Applish::Exception',
        alias   => 'exception',
    },

    {
        class   => 'Applish::Exception::ImportError',
        isa     => 'Applish::Exception',
        alias   => 'import_error',
        fields  => [qw( require )],
    },
    {
        class   => 'Applish::Exception::LibraryImportError',
        isa     => 'Applish::Exception::ImportError',
        alias   => 'library_import_error',
        fields  => [qw( require )],
    },

    {
        class   => 'Applish::Exception::EvaluateError',
        isa     => 'Applish::Exception',
        alias   => 'evaluate_error',
    },

    {
        class   => 'Applish::Exception::ParameterMissing',
        isa     => 'Applish::Exception',
        alias   => 'parameter_missing',
    },
    {
        class   => 'Applish::Exception::ArgumentParameterMissing',
        isa     => 'Applish::Exception::ParameterMissing',
        alias   => 'argument_parameter_missing',
    },

    {
        class   => 'Applish::Exception::InvalidParameter',
        isa     => 'Applish::Exception',
        alias   => 'invalid_parameter',
    },
    {
        class   => 'Applish::Exception::InvalidArgumentParameter',
        isa     => 'Applish::Exception::InvalidParameter',
        alias   => 'invalid_argument_parameter',
    },

    {
        class   => 'Applish::Exception::NotSupported',
        isa     => 'Applish::Exception',
        alias   => 'not_supported',
    }
);

for my $task ( @tasks ) {
    my $class   = $task->{'class'};
    my $alias   = $task->{'alias'};
    my $fields  = $task->{'fields'} || [];

    my %args    = ( error => "test exception" );
    @args{@{ $fields }} = map { 'error' } @{ $fields };

    local $@;
    eval {
        no strict 'refs';
        &{$alias}( %args );
    };

    if ( ! ref $@ ) {
        die $@;
    }

    isa_ok( $@, $class );
    isa_ok( $@, $task->{'isa'} ) if ( exists $task->{'isa'} );

    for my $name ( @{ $fields } ) {
        is( $@->$name(), 'error' );
    }
}

done_testing;
