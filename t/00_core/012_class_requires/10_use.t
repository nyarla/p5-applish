#!perl

use strict;
use warnings;

package Foo;

use Test::More;

use Applish::Class;
use Applish::Class::Engine qw( any_moose is_class_loaded );
use Applish::Class::Requires (
    '::Util',
);

ok( is_class_loaded( any_moose('::Util') ) );

done_testing;
