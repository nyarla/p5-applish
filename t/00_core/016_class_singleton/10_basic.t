#!perl

use strict;
use warnings;

use Test::More;

{
    package MySingleton;
    
    use strict;
    use warnings;
    
    use base qw/Applish::Class::Singleton/;
    
    sub class { 'MyClass' }
    
    1;
    
    package MyClass;
    
    sub new { bless {}, shift }
    
    1;
}

is(
    MySingleton->new,
    MySingleton->instance,
);

done_testing;
