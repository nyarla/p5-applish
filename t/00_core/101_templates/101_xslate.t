#!perl

use strict;
use warnings;

use Test::More;
use Test::Requires qw[ Text::Xslate ];
use Applish::Test::PathUtils qw( $examples );

BEGIN { use_ok('Applish::Template::Xslate') }

my $root = "${examples}/core.template.xslate";
my $tmpl = Applish::Template::Xslate->new( path => [ $root ] );

isa_ok( $tmpl, 'Applish::Template::Xslate' );

ok( $tmpl->exists('index') );
ok( ! $tmpl->exists('notfound') );

my $stat = $tmpl->stat('index');

is_deeply(
    $stat,
    {
        fullpath        => "${root}/index.xt",
        created         => File::stat::stat("${root}/index.xt")->ctime,
        lastmodified    => File::stat::stat("${root}/index.xt")->mtime,
    }
);

$stat = $tmpl->stat('notfound');
is( $stat, undef );

is(
    $tmpl->render('index', name => 'foo'),
    'hello foo',
);


is(
    $tmpl->flavour('/path/to/index', name => 'bar'),
    'hello bar',
);

done_testing;
