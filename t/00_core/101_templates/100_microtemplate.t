#!perl

use strict;
use warnings;

use Test::More;
use Test::Requires qw( Text::MicroTemplate::Extended );
use Applish::Test::PathUtils qw( $examples );

BEGIN { use_ok('Applish::Template::MicroTemplate') }

my $root = "${examples}/core.template.microtemplate";
my $tmpl = Applish::Template::MicroTemplate->new( include_path => [ $root ] );

isa_ok( $tmpl, 'Applish::Template::MicroTemplate' );

ok( $tmpl->exists('index') );
ok( ! $tmpl->exists('notfound') );

my $stat = $tmpl->stat('index');

is_deeply(
    $stat,
    {
        fullpath        => "${root}/index.mt",
        created         => File::stat::stat("${root}/index.mt")->ctime,
        lastmodified    => File::stat::stat("${root}/index.mt")->mtime,
    }
);

$stat = $tmpl->stat('notfound');

is( $stat, undef );

is(
    $tmpl->render('index', 'foo'),
    'hello foo',
);

is(
    $tmpl->flavour('/path/to/index', 'bar'),
    'hello bar',
);

done_testing;
