#!perl

use strict;
use warnings;

use Test::More;
use Test::Requires qw[ Data::Dumper ];

BEGIN { use_ok('Applish::Format::Perl') }

my $format = Applish::Format::Perl->new;

is_deeply(
    $format->Load( q{ { foo => 'bar' } } ),
    { foo => 'bar' },
);

is(
    eval( $format->Dump( sub { return 'foo' } ) )->(),
    'foo',
);

ok( $format->is_supported_file('file.pl') );
ok( $format->is_supported_file('file.perl') );

done_testing;
