#!perl

use strict;
use warnings;

use Test::More;
use Applish::Variables;

my $vars = Applish::Variables->new({ foo => '日本語' });

ok( ! utf8::is_utf8($vars->{'foo'}) );

$vars->decode('utf8');

ok( utf8::is_utf8($vars->{'foo'}) );

$vars->encode('utf8');

ok( ! utf8::is_utf8($vars->{'foo'}) );

done_testing;
