#!perl

use strict;
use warnings;

use Test::More;
use Applish::Variables;

my $vars = Applish::Variables->new({ foo => [qw( AAA BBB CCC )], bar => 'DDD', baz => { foo => 'EEE' }  });

$vars->visit(sub { lc $_[0] });

is_deeply(
    $vars,
    {
        foo => [qw( aaa bbb ccc )],
        bar => 'ddd',
        baz => { foo => 'eee' },
    },
);

done_testing;
