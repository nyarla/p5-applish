#!perl

use strict;
use warnings;

use Test::More;
use Applish::Variables;

my $vars = Applish::Variables->new;

isa_ok( $vars, 'Applish::Variables' );
is_deeply( $vars, {} );

$vars = Applish::Variables->new( { foo => 'bar' } );

isa_ok( $vars, 'Applish::Variables' );
is_deeply( $vars, { foo => 'bar' } );

done_testing;
