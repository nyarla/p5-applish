#!perl

use strict;
use warnings;

use Test::More;
use Applish::Variables;

my $vars = Applish::Variables->new;

$vars->set('foo' => 'bar');

is( $vars->get('foo'), 'bar' );

done_testing;
