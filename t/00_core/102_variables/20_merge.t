#!perl

use strict;
use warnings;

use Test::More;
use Applish::Variables;

my $vars = Applish::Variables->new({ baz => { foo => 'AAA' } });

$vars->merge({ foo => 'AAA' }, { bar => [qw( AAA BBB )] }, { baz => { bar => 'BBB', baz => 'CCC' } });

is_deeply(
    $vars,
    {
        foo => 'AAA',
        bar => [qw( AAA BBB )],
        baz => {
            foo => 'AAA',
            bar => 'BBB',
            baz => 'CCC',
        },
    },
);

done_testing;
